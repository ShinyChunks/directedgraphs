package de.unileipzig.biovis.graph;

import java.util.LinkedList;
import java.util.List;

/**
 * Class representing a graph.
 * 
 * @author chris
 *
 */
public class Graph {

	private List<Edge> edges;

	private List<Node> nodes;
	
	/**
	 * Constructor
	 */
	public Graph(List<Edge> edges, List<Node> nodes) {
		this.nodes = nodes;
		this.edges = edges;
	}

	/**
	 * Gets the edges of the graph.
	 * 
	 * @return A list of edges of the graph.
	 */
	public List<Edge> getEdges() {
		return edges;
	}

	/**
	 * Sets the edges of the graph.
	 * 
	 * @param edges
	 *            A list of edges.
	 */
	public void setEdges(List<Edge> edges) {
		this.edges = edges;
	}

	/**
	 * Gets the nodes of the graph.
	 * 
	 * @return A list of nodes of the graph.
	 */
	public List<Node> getNodes() {
		return nodes;
	}

	/**
	 * Sets the nodes of the graph.
	 * 
	 * @param nodes
	 *            A list of nodes to set as the nodes of the graph.
	 */
	public void setNodes(List<Node> nodes) {
		this.nodes = nodes;
	}

	

	/**
	 * Adds a edge from node "from" to node "to"
	 * 
	 * @param from
	 *            Node where the edge starts.
	 * @param to
	 *            Node where the edge ends.
	 */
	public void addEdge(Node from, Node to) {
		edges.add(new Edge(from, to));
	}

	/**
	 * Adds a node with incoming and outgoing edges.
	 * 
	 * @param incomingEdges
	 *            The list of incoming edges.
	 * @param outgoingEdges
	 *            The list of outgoing edges.
	 */
	public void addNode(List<Edge> incomingEdges, List<Edge> outgoingEdges) {
		nodes.add(new Node(incomingEdges, outgoingEdges));
	}
	
	
	
	/**
	 * makes a copy of this graph
	 * @return copy of graph
	 */
	public Graph copy(){
		Graph graphcopy = new Graph(copyEdges(),copyNodes());
		return graphcopy;
	}
	
	/**
	 * copies the edges
	 * @return copy of edge list
	 */
	private List<Edge> copyEdges(){
		List<Edge> edgescopy = new LinkedList<Edge>();
		for (Edge edge : edges){
			edgescopy.add(edge);
		}
		return edgescopy;
	}
	
	/**
	 * copies the nodes
	 * @return copy of node list
	 */
	private List<Node> copyNodes(){
		List<Node> nodescopy = new LinkedList<Node>();
		for (Node node : nodes){
			nodescopy.add(node);
		}
		return nodescopy;
	}
	
	@Override
	public String toString() {
		return edges + "\n" + nodes;
	}
}
