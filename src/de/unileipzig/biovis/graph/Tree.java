package de.unileipzig.biovis.graph;

import java.util.LinkedList;
import java.util.List;

/**
 * Class representing a tree. This extends the class Graph.
 * 
 * @author chris
 *
 */
public class Tree extends Graph {

	private Node root;

	/**
	 * Constructor.
	 * 
	 * @param edges
	 *            The list of edges.
	 * @param nodes
	 *            The list of nodes.
	 * @param root
	 *            A special node that represents the root element of the tree.
	 */
	public Tree(List<Edge> edges, List<Node> nodes, Node root) {
		super(edges, nodes);
		this.setRoot(root);
	}

	/**
	 * Gets the root node of the tree.
	 * 
	 * @return The root node of the tree.
	 */
	public Node getRoot() {
		return root;
	}

	/**
	 * Set a node as root element of the tree.
	 * 
	 * @param root
	 *            The node to set as root element.
	 */
	public void setRoot(Node root) {
		this.root = root;
	}

	@Override
	public String toString() {
		List<String> treeStringList = new LinkedList<String>();
		treeStringList.add(getRoot().getName());
		addChildren(getRoot(),treeStringList,  0);
		String treeString = "Tree has " + treeStringList.size() + " nodes with " + getRoot().getName() + " as the root node.\n";
		for(String str : treeStringList) {
			treeString += str + "\n";
		}
		return treeString;
	}

	private List<String> addChildren(Node node,List<String> treeStringList, int depth) {
		LinkedList<Node> children = node.getChildren();
		while(!children.isEmpty()) {
			String nodeString = "";
			for(int i = 0; i <= depth; i++) {
				nodeString += "  ";
			}
			Node childNode = children.removeFirst();
			nodeString += "|_ " + childNode.getName();
			treeStringList.add(nodeString);
			addChildren(childNode, treeStringList, depth + 1);
		}
		return treeStringList;
	}
}
