package de.unileipzig.biovis.graph;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;


public class GreedyAcyclicGraph extends Graph{
	private Graph graphcopy;
	private List<Node> nodes;
	private List<Edge> edges;
	private List<Edge> acyclicEdges = new LinkedList<Edge>();
	private List<Edge> reverseEdges = new LinkedList<Edge>();
	private List<Edge> removedEdges = new LinkedList<Edge>();
	private List<Node> newNodes = new LinkedList<Node>();
	private List<DummyNode> dummyNodes;
	private List<Edge> finalEdges = new LinkedList<>();

	
	public GreedyAcyclicGraph(Graph graph){
		super(graph.getEdges(), graph.getNodes());
		this.graphcopy = graph.copy();
		this.nodes = graphcopy.getNodes();
		this.edges = graphcopy.getEdges();
		dummyNodes = new LinkedList<DummyNode>();
		run();
		// add reversible edges, ggf auskommentieren
		addReversibleEdges();

		setNodeEdges();
	}
	
	public void run(){
		while(!nodes.isEmpty()){
			removeSources();
			removeSinks();
			
			if(!nodes.isEmpty()){
				removeMaxDifNode();
			}
		}
		
	}

	
	public List<Edge> getRemovedEdges() {
		return removedEdges;
	}

	public List<Edge> getReverseEdges() {
		return reverseEdges;
	}
	/**
	 * remove node with maximal difference of number of incoming and outgoing edges
	 * and add node to acyclic graph and its incoming edges
	 */
	
	private void removeMaxDifNode(){
		Node node = getMaxEdgeDifNode();
		List<Edge> in = node.getEdgesIn();
		acyclicEdges.addAll(in);
		newNodes.add(node);
		removeEdges(in);
		reverseEdges.addAll(node.getEdgesOut());
		removeEdges(node.getEdgesOut());
		nodes.remove(node);
		
	}
	
	/**
	 * save all edges which are not contained in a direct loop in reverseEdges
	 * and reverse them
	 * @param edges
	 */
	
	private void addReversibleEdges(){
		
		List<Edge> reverseList = new LinkedList<Edge>();
		for (Edge edge : reverseEdges){
			if (!acyclicEdges.contains(edge)){
				
				if(isreversible(edge)){
					edge.reverse();
					reverseList.add(edge);
				}
				else
					removedEdges.add(edge);
			}
		}
		reverseEdges = reverseList;
		acyclicEdges.addAll(reverseEdges);
		
	}
	/**
	 * check if edge is reversible, e.g. contains not to a direct loop
	 * @param edge
	 * @return 
	 */
	
	private boolean isreversible(Edge edge){
		Node start = edge.getStart();
		Node end = edge.getEnd();
		for (Edge e : acyclicEdges){
			if(e.getStart().equals(end) && e.getEnd().equals(start)){
				return false;
			}
		}
		return true;
	}
	
	/**
	 * find node with maximal difference of number of incoming and outgoing edges
	 * @return this node
	 */
	
	private Node getMaxEdgeDifNode(){
		int dif = 0;
		int maxdif = 0;
		Node finalnode = null;
		for (Node node : nodes){
			dif =Math.abs(node.getEdgesIn().size()-node.getEdgesOut().size());
			if (dif>=maxdif){
				maxdif = dif;
				finalnode = node;
			}
		}
		return finalnode;
	}
	
	
	/**
	 * remove all nodes without children from node list
	 * and add them to the acyclic Graph
	 */
	private void removeSinks(){
		boolean sink = true;
		while(sink){
			
			sink = false;
			List<Node> removals = new LinkedList<Node>();
			for (Node node : nodes){
				if(node.getChildren().isEmpty()){
					sink = true;
					removals.add(node);
					acyclicEdges.addAll(node.getEdgesIn());
					removeEdges(node.getEdgesIn());
				}
			}
			nodes.removeAll(removals);
			newNodes.addAll(removals);
			
		}
	}
	/**
	 * removes all nodes without incoming edges
	 * and add them to the acyclic graph
	 */
	
	private void removeSources(){
		boolean source = true;
		while(source){
			source = false;
			List<Node> removals = new LinkedList<Node>();
			for (Node node : nodes){
				if(node.getEdgesIn().isEmpty()){
					removals.add(node);
					acyclicEdges.addAll(node.getEdgesOut());
					removeEdges(node.getEdgesOut());
					source = true;
				}
			}
			nodes.removeAll(removals);
			newNodes.addAll(removals);
		}
	}
	

	
	/**
	 * removes edges from nodes and edges 
	 * and add them to acyclic edge list
	 * @param edges list of edges to be removed
	 */
	private void removeEdges(List<Edge> edges){
		
		this.edges.removeAll(edges);
		while (!edges.isEmpty()){
			edges.get(0).remove();
		}
	}
	
	@Override
	public List<Edge> getEdges() {
		return acyclicEdges;
	}

	@Override
	public List<Node> getNodes() {
		return newNodes;
	}
	
	private void setNodeEdges(){
		Node start;
		Node end;
		for (Edge edge : acyclicEdges){
			start = edge.getStart();
			end = edge.getEnd();
			if (!start.getEdgesOut().contains(edge))
				edge.getStart().addOutgoingEdge(edge);
			if (!end.getEdgesIn().contains(edge))
				edge.getEnd().addIncomingEdge(edge);
		}
	}
	/**
	 * Adds a node with incoming and outgoing edges.
	 * 
	 * @param incomingEdge
	 *            The incoming edge.
	 * @param outgoingEdge
	 *            The outgoing edge.
	 */
	public void addDummyNode(Edge incomingEdge, Edge outgoingEdge) {
		DummyNode dummy = new DummyNode(incomingEdge, outgoingEdge);
		nodes.add(dummy);
		dummyNodes.add(dummy);
	}
	
	/**
	 * Adds a dummy node.
	 * @param dummy
	 */
	public void addDummyNode(DummyNode dummy) {
		if (!acyclicEdges.contains(dummy.getEdgesIn().get(0))){
				acyclicEdges.addAll(dummy.getEdgesIn());
		}
		acyclicEdges.addAll(dummy.getEdgesOut());
		nodes.add(dummy);
		dummyNodes.add(dummy);
	}
	
	public List<DummyNode> getDummyNodes() {
		return dummyNodes;
	}

	public void setDummyNodes(List<DummyNode> dummyNodes) {
		this.dummyNodes = dummyNodes;
	}
	
	/**
	 * makes a copy of this graph
	 * @return copy of graph
	 */
	public GreedyAcyclicGraph copy(){
		Graph graphcopy = super.copy();
		return new GreedyAcyclicGraph(graphcopy);
	}
	
	
	/**
	 * get original edges with dummy nodes
	 * @param edges
	 * @return
	 */
	public List<Edge> getOriginalDummyEdges(List<Edge> edges){
		finalEdges = new LinkedList<>();
		List<Edge> dummyEdges = new LinkedList<>();
		for (Edge edge : acyclicEdges){
			if (edges.contains(edge)){
				finalEdges.add(edge);
			}
			else if (reverseEdges.contains(edge)){
				finalEdges.add(new Edge(edge.getEnd(),edge.getStart()));
			}
			else{
				dummyEdges.add(edge);
			}
			
		}
		//System.out.println(dummyEdges);
		addDummyEdges(edges, dummyEdges);
		
		finalEdges.addAll(removedEdges);
		
		return finalEdges;
	}

	/**
	 * add dummy edges to final edges in orginal direction
	 * @param edges
	 * @param dummyEdges
	 */
	private void addDummyEdges(List<Edge> edges, List<Edge> dummyEdges) {
		List<Edge> alignEdges;
		Node start, end;
		for (Edge edge : dummyEdges){
			if (!edge.getEnd().getClass().equals(DummyNode.class)){
				end = edge.getEnd();
				alignEdges  = new LinkedList<>();
				alignEdges.add(edge);
				while (edge.getStart().getClass().equals(DummyNode.class)){
					edge = ((DummyNode)edge.getStart()).getEdgeIn();
					alignEdges.add(0,edge);
					
				}
				start = edge.getStart();
				boolean reverse = false;
				for (Edge revEdge :	reverseEdges){
					if (revEdge.getStart().equals(start) && revEdge.getEnd().equals(end)){
						reverse = true;
						addReversedEdgeList(alignEdges);
					}
				}
				if (! reverse){
					for (Edge gEdge : edges){
						if (gEdge.getStart().equals(start) && gEdge.getEnd().equals(end)){
							reverse = false;
							finalEdges.addAll(alignEdges);
							
						}
					}
				}
				
			}
		}
	}
	
	/**
	 * add reversed edges to final edges
	 * @param alignEdges
	 */
	private void addReversedEdgeList(List<Edge> alignEdges){
		for (Edge edge : alignEdges){
			finalEdges.add(new Edge(edge.getEnd(),edge.getStart()));
		}
		
	}

}
