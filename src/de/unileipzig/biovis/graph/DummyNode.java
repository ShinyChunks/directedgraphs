package de.unileipzig.biovis.graph;

import java.util.Arrays;
import java.util.LinkedList;

public class DummyNode extends Node {
	
	private Node startNode;
	
	private Node endNode;
	
	private Edge edgeIn;
	private Edge edgeOut;

	public Edge getEdgeIn() {
		return edgeIn;
	}

	public void setEdgeIn(Edge edgeIn) {
		this.edgeIn = edgeIn;
	}

	public Edge getEdgeOut() {
		return edgeOut;
	}

	public void setEdgeOut(Edge edgeOut) {
		this.edgeOut = edgeOut;
	}

	public DummyNode(String name) {
		super(name);
	}
	
	public DummyNode(LinkedList<Edge> incomingEdges, LinkedList<Edge> outgoingEdges) {
		super(incomingEdges, outgoingEdges);
		edgeIn = incomingEdges.getFirst();
		edgeOut = outgoingEdges.getFirst();
	}
	
	
	public DummyNode() {
		super("DummyNode");
	}
	
	/**
	 * Constructor.
	 * @param in The incoming edge.
	 * @param out The outgoing edge.
	 */
	public DummyNode(Edge in, Edge out) {
		super(Arrays.asList(in), Arrays.asList(out));
		edgeIn = in;
		edgeOut = out;
	}
	
	public DummyNode(LinkedList<Edge> edgesIn, LinkedList<Edge> edgesOut, String name) {
		super(edgesIn, edgesOut, name);
		edgeIn = edgesIn.getFirst();
		edgeOut = edgesOut.getFirst();
	}
	
	
	/**
	 * Adds a incoming edge.
	 * 
	 * @param edge
	 *            The incoming edge to be added.
	 */
	public void addIncomingEdge(Edge edge) {
		super.getEdgesIn().add(edge);
		edgeIn = edge;
	}

	/**
	 * Adds a outgoing edge.
	 * 
	 * @param edge
	 *            The outgoing edge to be added.
	 */
	public void addOutgoingEdge(Edge edge) {
		super.getEdgesOut().add(edge);
		edgeOut = edge;
	}
	
}
