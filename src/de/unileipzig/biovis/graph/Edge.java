package de.unileipzig.biovis.graph;

/**
 * Class representing a edge of a graph.
 * @author chris
 *
 */
public class Edge {
	
	private Node start, end;
	
	private double weight;
	
	//necessary for positioning
	private boolean marked = false;
	
	

	/**
	 * Constructor.
	 * @param start The node where the edge starts.
	 * @param end The node where the edge ends.
	 */
	public Edge(Node start, Node end) {
		setStart(start);
		setEnd(end);
		//default weight is set to 1
		setWeight(1.0);
	}
	
	/**
	 * Constructor.
	 * @param start The node where the edge starts.
	 * @param end The node where the edge ends.
	 */
	public Edge(Node start, Node end, double weight) {
		setStart(start);
		setEnd(end);
		setWeight(weight);
	}

	/**
	 * Gets the start node of the edge.
	 * @return The node where the edge starts.
	 */
	public Node getStart() {
		return start;
	}

	/**
	 * Set the start node of the edge.
	 * @param start The node to set as the start of the edge.
	 */
	public void setStart(Node start) {
		this.start = start;
	}

	/**
	 * Gets the end node of the edge.
	 * @return The node where the edge ends.
	 */
	public Node getEnd() {
		return end;
	}

	/**
	 * Sets the end node of the edge.
	 * @param end The node to set as the end of the edge.
	 */
	public void setEnd(Node end) {
		this.end = end;
	}

	/**
	 * Gets the weight of the edge. For unweighted graphs this is most likely 1.0.
	 * @return The weight of the edge.
	 */
	public double getWeight() {
		return weight;
	}

	/**
	 * Sets the weight of the edge. Set this to 1.0 for unweighted graphs.
	 * @param weight The weight to set for the edge.
	 */
	public void setWeight(double weight) {
		this.weight = weight;
	}
	/**
	 * reverses edge, switch start and and node
	 */
	public void reverse(){
		Node child = this.getEnd();
		Node parent = this.getStart();
		this.setEnd(parent);
		this.setStart(child);	
		
		child.getEdgesIn().remove(this);
		child.getEdgesOut().add(this);
		parent.getEdgesOut().remove(this);
		parent.getEdgesIn().add(this);
		
		
	}
	
	@Override
	public String toString() {
		return start.getName() + " - " + end.getName();
	}

	public void remove() {
		Node child = this.getEnd();
		Node parent = this.getStart();
		child.getEdgesIn().remove(this);
		parent.getEdgesOut().remove(this);

		
	}
	
	public boolean isMarked() {
		return marked;
	}

	public void setMarked(boolean marked) {
		this.marked = marked;
	}
	
}
