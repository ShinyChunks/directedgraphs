package de.unileipzig.biovis.graph;

import java.util.LinkedList;
import java.util.List;

/**
 * Class representing a node of a graph.
 * 
 * @author chris
 *
 */
public class Node {

	private List<Edge> edgesIn;

	private List<Edge> edgesOut;

	private String name;

	private String additionalInformation;
	
	private int label = 0;
	
	private double x_coord;
	
	private int y_coord;

	

	public double getX_coord() {
		return x_coord;
	}

	public void setX_coord(double x_coord) {
		this.x_coord = x_coord;
	}

	public int getY_coord() {
		return y_coord;
	}

	public void setY_coord(int y_coord) {
		this.y_coord = y_coord;
	}

	/**
	 * Constructor. Use null for parameters, if there are no outgoing or
	 * incoming edges.
	 * 
	 * @param incomingEdges
	 *            List of incoming edges.
	 * @param outgoingEdges
	 *            List of outgoing edges.
	 */
	public Node(List<Edge> incomingEdges, List<Edge> outgoingEdges) {
		setEdgesIn(incomingEdges);
		setEdgesOut(outgoingEdges);
	}

	/**
	 * Constructor. Use null for parameters, if there are no outgoing or
	 * incoming edges.
	 * 
	 * @param edgesIn
	 *            List of incoming edges.
	 * @param edgesOut
	 *            List of outgoing edges.
	 * @param name
	 *            The name of the node.
	 */
	public Node(LinkedList<Edge> edgesIn, LinkedList<Edge> edgesOut, String name) {
		setEdgesIn(edgesIn);
		setEdgesOut(edgesOut);
		setName(name);
	}

	/**
	 * Constructor.
	 * 
	 * @param name
	 *            The name of the node.
	 */
	public Node(String name) {
		this(new LinkedList<Edge>(), new LinkedList<Edge>(), name);
	}

	/**
	 * Adds a incoming edge.
	 * 
	 * @param edge
	 *            The incoming edge to be added.
	 */
	public void addIncomingEdge(Edge edge) {
		edgesIn.add(edge);
	}

	/**
	 * Adds a outgoing edge.
	 * 
	 * @param edge
	 *            The outgoing edge to be added.
	 */
	public void addOutgoingEdge(Edge edge) {
		edgesOut.add(edge);
	}

	/**
	 * Gets the incoming edges of the node.
	 * 
	 * @return The list of incoming edges.
	 */
	public List<Edge> getEdgesIn() {
		return edgesIn;
	}

	/**
	 * Sets the incoming edges of the node.
	 * 
	 * @param incomingEdges
	 *            A list of edges to set as incoming edges of the node.
	 */
	public void setEdgesIn(List<Edge> incomingEdges) {
		this.edgesIn = incomingEdges;
	}

	/**
	 * Get the list of outgoing edges.
	 * 
	 * @return The list of outgoing edges of the node.
	 */
	public List<Edge> getEdgesOut() {
		return edgesOut;
	}

	/**
	 * Sets the outgoing edges of the node.
	 * 
	 * @param outgoingEdges
	 *            A list of nodes to set as outgoing edges.
	 */
	public void setEdgesOut(List<Edge> outgoingEdges) {
		this.edgesOut = outgoingEdges;
	}
	
	/**
	 * Gets the label of the node.
	 * 
	 * @return an integer representing the label of the node.
	 */
	
	public int getLabel() {
		return label;
	}
	
	/**
	 * Sets the label of the node.
	 * 
	 * @param label
	 *            an integer to set as the node's label.
	 */

	public void setLabel(int label) {
		this.label = label;
	}

	/**
	 * Gets the name of the node.
	 * 
	 * @return A string representing the name of the node.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name of the node.
	 * 
	 * @param name
	 *            A string to set as the node's name.
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets a string with additional information if available.
	 * 
	 * @return A string of additional information if available.
	 */
	public String getAdditionalInformation() {
		return additionalInformation;
	}

	/**
	 * Set a string as the additional information of the node. Can be used to
	 * save the type of edge.
	 * 
	 * @param additionalInformation
	 */
	public void setAdditionalInformation(String additionalInformation) {
		this.additionalInformation = additionalInformation;
	}

	/**
	 * Returns the children of the node.
	 * 
	 * @return List of children.
	 */
	public LinkedList<Node> getChildren() {
		LinkedList<Node> children = new LinkedList<Node>();
		for (Edge e : edgesOut) {
			children.add(e.getEnd());
		}
		return children;
	}

	/**
	 * Adds a child node by adding a new edge from this node to the child node.
	 * 
	 * @param childNode
	 *            The node to be added as a child.
	 */
	public void addChildren(Node childNode) {
		edgesOut.add(new Edge(this, childNode));
		childNode.addIncomingEdge(new Edge(this, childNode));
	}

	/**
	 * Clear all children by deleting all outgoing edges.
	 */
	public void clearChildren() {
		edgesOut.clear();
	}

	@Override
	public String toString() {
		String str = name;
		// if(!edgesOut.isEmpty()) {
		// for(Edge e : edgesOut) {
		// str += " |_ " + e.getEnd() + "\n";
		// }
		// }
		return str;
	}
	
	
}
