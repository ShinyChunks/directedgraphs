package de.unileipzig.biovis.ui;

import de.unileipzig.biovis.xmlimport.XMLImporter;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.stage.Stage;

public class TypePopupController {

    @FXML
    private Button btOk;

    @FXML
    private Button btCancel;

    @FXML
    private ChoiceBox<String> cbEdgeTypes;
    
    private MainPanelController mainPanelController;
    
    private Stage stage;

    /**
     * Closes the popup.
     * @param event
     */
    @FXML
    void handleBtCancel(ActionEvent event) {
    	stage.close();
    }

    /**
     * Sets the edge type and closes the popup window.
     * @param event
     */
    @FXML
    void handleBtOk(ActionEvent event) {
    	mainPanelController.setEdgeType(cbEdgeTypes.getValue());
    	stage.close();
    }
    
    /**
     * Initializes the controller after creating all FXML components.
     */
    @FXML
    public void initialize() {
    	for(String s : XMLImporter.EDGE_TYPES) {
    		cbEdgeTypes.getItems().add(s);
    	}
    }
    
    /**
     * Sets the MainPanelController instance to the given controller. This is used to set the edge type.
     * @param controller
     */
    public void setMainPanelController(MainPanelController controller) {
    	mainPanelController = controller;
    }
    
    /**
     * Sets the stage of the popup so that the stage can be closed with the buttons.
     * @param stage
     */
    public void setStage(Stage stage) {
    	this.stage = stage;
    }
}
