package de.unileipzig.biovis.ui;

import javafx.beans.InvalidationListener;
import javafx.beans.property.DoubleProperty;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;

public class Arrow extends Group {

    private final Line line;
    
    private final Line arrow1;
    
    private final Line arrow2;
    
    private boolean isDummyEdge;
    
    private double strokeWidth;

	/**
     * 
     */
    public Arrow() {
        this(new Line(), new Line(), new Line());
    }
    
    /**
     * Preferred constructor.
     * The line indicates the edge with start and end.
     * @param line The line representing the edge from start point to end point.
     */
    public Arrow(Line line) {
    	this(line, new Line(), new Line());
    }

    private static final double ARROW_LENGTH = 15;
    private static final double ARROW_WIDTH = 7;

    /**
     * The actual constructor with the edge line and the two lines for the arrow head.
     * @param line The actual edge line.
     * @param arrow1 The first part of the arrow head.
     * @param arrow2 The second part of the arrow head.
     */
    private Arrow(Line line, Line arrow1, Line arrow2) {
        super(line, arrow1, arrow2);
        this.line = line;
        this.arrow1 = arrow1;
        this.arrow2 = arrow2;
        InvalidationListener updater = o -> {
            double ex = getEndX();
            double ey = getEndY();
            double sx = getStartX();
            double sy = getStartY();

            arrow1.setEndX(ex);
            arrow1.setEndY(ey);
            arrow2.setEndX(ex);
            arrow2.setEndY(ey);

            if (ex == sx && ey == sy) {
                // arrow parts of length 0
                arrow1.setStartX(ex);
                arrow1.setStartY(ey);
                arrow2.setStartX(ex);
                arrow2.setStartY(ey);
            } else {
                double factor = ARROW_LENGTH / Math.hypot(sx-ex, sy-ey);
                double factorO = ARROW_WIDTH / Math.hypot(sx-ex, sy-ey);

                // part in direction of main line
                double dx = (sx - ex) * factor;
                double dy = (sy - ey) * factor;

                // part ortogonal to main line
                double ox = (sx - ex) * factorO;
                double oy = (sy - ey) * factorO;

                arrow1.setStartX(ex + dx - oy);
                arrow1.setStartY(ey + dy + ox);
                arrow2.setStartX(ex + dx + oy);
                arrow2.setStartY(ey + dy - ox);
            }
        };

        // add updater to properties
        startXProperty().addListener(updater);
        startYProperty().addListener(updater);
        endXProperty().addListener(updater);
        endYProperty().addListener(updater);
        updater.invalidated(null);
    }

    // start/end properties

    public final void setStartX(double value) {
        line.setStartX(value);
    }

    public final double getStartX() {
        return line.getStartX();
    }

    public final DoubleProperty startXProperty() {
        return line.startXProperty();
    }

    public final void setStartY(double value) {
        line.setStartY(value);
    }

    public final double getStartY() {
        return line.getStartY();
    }

    public final DoubleProperty startYProperty() {
        return line.startYProperty();
    }

    public final void setEndX(double value) {
        line.setEndX(value);
    }

    public final double getEndX() {
        return line.getEndX();
    }

    public final DoubleProperty endXProperty() {
        return line.endXProperty();
    }

    public final void setEndY(double value) {
        line.setEndY(value);
    }

    public final double getEndY() {
        return line.getEndY();
    }

    public final DoubleProperty endYProperty() {
        return line.endYProperty();
    }

	public void setFill(Color color) {
		line.setFill(color);
		arrow1.setFill(color);
		arrow2.setFill(color);
	}
	
	public void hideArrowHead() {
		getChildren().removeAll(arrow1, arrow2);
	}
	
	public void showArrowHead() {
		getChildren().addAll(arrow1, arrow2);
	}
    
    public boolean isDummyEdge() {
		return isDummyEdge;
	}

	public void setIsDummyEdge(boolean isDummyEdge) {
		this.isDummyEdge = isDummyEdge;
	}

	public double getStrokeWidth() {
		return strokeWidth;
	}

	public void setStrokeWidth(double strokeWidth) {
		this.strokeWidth = strokeWidth;
		line.setStrokeWidth(strokeWidth);
		arrow1.setStrokeWidth(strokeWidth);
		arrow2.setStrokeWidth(strokeWidth);
	}
}
