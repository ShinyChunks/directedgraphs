package de.unileipzig.biovis.ui;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import de.unileipzig.biovis.MainApp;
import de.unileipzig.biovis.Preferences;
import de.unileipzig.biovis.ecm.LayerAssignment;
import de.unileipzig.biovis.ecm.MultiLayerCrossMinimization;
import de.unileipzig.biovis.ecm.OneLayerCrossMinimization;
import de.unileipzig.biovis.ecm.VertexPositioning;
import de.unileipzig.biovis.graph.*;
import de.unileipzig.biovis.xmlimport.XMLImporter;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tooltip;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.text.Text;
import javafx.scene.transform.Scale;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;

public class MainPanelController {
	
	@FXML
	private MenuBar menuBar;

	@FXML
	private Menu menuFile;
	
	@FXML
	private MenuItem menuItemClose;
	
	@FXML
	private MenuItem menuItemOpenFile;
	
	@FXML
	private MenuItem menuItemPreferences;

	@FXML
	private Menu menuView;
	
    @FXML
    private Menu menuZoom;

    @FXML
    private MenuItem menuItemZoom25;

    @FXML
    private MenuItem menuItemZoom50;

    @FXML
    private MenuItem menuItemZoom75;

    @FXML
    private MenuItem menuItemZoom100;

    @FXML
    private MenuItem menuItemZoom150;

    @FXML
    private MenuItem menuItemZoom200;
    
    @FXML
    private MenuItem menuItemZoom400;
    
    @FXML
    private Menu menuGraph;
    
    @FXML
    private CheckMenuItem menuItemDrawOriginalEdges;
	
    @FXML
    private CheckMenuItem menuItemShowEdges;
    
    @FXML
    private CheckMenuItem menuItemShowLabels;
    
    @FXML
    private CheckMenuItem menuItemHighlightDummyNodes;

	@FXML
	private Menu menuEdit;
	
    @FXML
    private MenuItem menuItemShowUnoptimized;
	
    @FXML
    private MenuItem menuItemRemoveCycles;
    
    @FXML
    private MenuItem menuItemAssignLayer;
    
    @FXML
    private Menu menuOLCM;

    @FXML
    private MenuItem menuItemOLCMBarycenter;
    
    @FXML
    private MenuItem menuItemMLCM;
    
    @FXML
    private Menu menuPositioning;
    
    @FXML
    private MenuItem menuItemPositioning;
    
    @FXML
    private MenuItem menuItemleftupper;
    
    @FXML
    private MenuItem menuItemleftlower;
    
    @FXML
    private MenuItem menuItemrightlower;
    
    @FXML
    private MenuItem menuItemrightupper;
    
    @FXML
    private CheckMenuItem menuItemShowDummyNodes;

    @FXML
    private CheckMenuItem menuItemStraightenEdges;
    
	@FXML
	private Menu menuHelp;

	@FXML
	private HBox hBoxStatusBar;
	
    @FXML
    private Label lbGraphInfo;
    
    @FXML
    private Label lbNodesEdges;

	@FXML
	private ScrollPane paneVisualization;

	private MainApp mainApp;
	
	private File sourceFile;
	
	private String edgeType = null;
	
	private LinkedList<Circle> nodeCircles = new LinkedList<Circle>();
	
	private LinkedList<Circle> dummyNodeCircles = new LinkedList<Circle>();
	
	private LinkedList<Arrow> edgeLines = new LinkedList<Arrow>();
	
	private LinkedList<Arrow> sourceEdges = new LinkedList<Arrow>();
	
	private LinkedList<Text> labels = new LinkedList<Text>();
	
	private LinkedList<Text> dummyLabels = new LinkedList<Text>();
	
	private double zoom = 1.0;
	
	/**
	 * The initially parsed, original graph.
	 */
//	private Graph parsedGraph;
	
//	/**
//	 * The graph without cycles.
//	 */
//	private GreedyAcyclicGraph acyclicGraph;
//	
//	/**
//	 * The graph without cycles and with dummy nodes.
//	 */
//	private GreedyAcyclicGraph modifiedGraph;
		
	private Group contentGroup = new Group();
	
	private Group zoomGroup = new Group();
	
	private Scale zoomScale;
	
	private XMLImporter xmlImport;
	
	private Preferences pref;
	
	private LayerAssignment layerAss;
	
	private GreedyAcyclicGraph dag;
	
	private List<Edge> origEdges;
	
	private int dummynodes;
	

	
    /**
     * Initializes the controller after creating all FXML components.
     */
    @FXML
    public void initialize() {
    	paneVisualization.addEventFilter(KeyEvent.KEY_PRESSED, e -> {
    		if(e.getCode().equals(KeyCode.PLUS)) {
    			zoom += .1;
    			zoom();
    		} else if(e.getCode().equals(KeyCode.MINUS)) {
    			if(zoom > 0) {
    				zoom -= .1;
    			}
    			zoom();
    		}
    	});
    	contentGroup.getChildren().add(zoomGroup);
    	zoomScale = new Scale(zoom, zoom);
    	zoomGroup.getTransforms().add(zoomScale);
    	paneVisualization.setContent(contentGroup);
    	pref = new Preferences();
    	initMenuTooltips();
    }

	private void initMenuTooltips() {
		//TODO add some tooltips to the menuitems
	}

	/**
	 * Set main app to interact with.
	 * @param mainApp The main app instance.
	 */
	public void setMainApp(MainApp mainApp) {
		this.mainApp = mainApp;
	}

	/**
	 * Close the application if the menu item "close" was pressed in the menu
	 * bar.
	 * 
	 * @param event
	 */
	@FXML
	void handleMenuItemClose(ActionEvent event) {
		mainApp.close();
	}
	
	/**
	 * Shows a preferences window
	 * @param event
	 */
	@FXML
	void handleMenuItemPreferences(ActionEvent event) {
		try {
            // Load the fxml file and create a new stage for the popup dialog.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("ui/Preferences.fxml"));
            BorderPane mainPanePref = (BorderPane) loader.load();

            // Create the dialog Stage.
            Stage stagePref = new Stage();
            stagePref.setTitle("Preferences...");
            stagePref.initModality(Modality.WINDOW_MODAL);
            stagePref.initOwner(mainApp.getPrimaryStage());
            Scene scenePref = new Scene(mainPanePref);
            scenePref.getStylesheets().add(MainApp.class.getResource("application.css").toString());
            stagePref.setScene(scenePref);

            PreferencesController prefController = loader.getController();
            prefController.setStage(stagePref);
            prefController.setMainAppController(this);
            prefController.setPreferences(pref);
            

            // Show the dialog and wait until the user closes it
            stagePref.showAndWait();

        } catch (IOException e) {
            e.printStackTrace();
        }
	}
	
	/**
	 * Opens a file chooser to choose which graphml file to load. It also adds the graph to the 
	 * visualization pane.
	 * @param event The action event that triggered this method.
	 */
	@FXML
	void handleMenuItemOpenFile(ActionEvent event) {
		clearLists();
		
		FileChooser chooser = new FileChooser();
		chooser.getExtensionFilters().add(new ExtensionFilter("GRAPHML-File (.graphml)", "*.graphml"));
		sourceFile = chooser.showOpenDialog(mainApp.mainWindow);
		xmlImport = new XMLImporter(sourceFile);
		//let the user choose which edge type to visualize as a graph
		showTypePopup();
		if(edgeType != null) {
			//get original graph
//			parsedGraph = xmlImport.parseToGraph(edgeType);
	    	
			//draw the graph initially without any optimization or style guideline
			zoomGroup.getChildren().add(drawGraphInitial(xmlImport.parseToGraph(edgeType)));
			mainApp.getPrimaryStage().setTitle(mainApp.getPrimaryStage().getTitle() + " - " 
			+ sourceFile.getName() + " - " + edgeType);
			enableMenuItems();
		} else {
			showWarning("Couldn't parse file to graph.", "There was a problem while parsing the file"
					+ " to graph. Please make sure you have choosen a correct edge type.", 
					"Try opening the file again and choose a edge type!");
		}
	}

	/**
	 * Enables the disabled menu items after loading a file.
	 */
	private void enableMenuItems() {
		menuZoom.setDisable(false);
		menuItemRemoveCycles.setDisable(false);
		menuItemAssignLayer.setDisable(false);
		menuItemShowUnoptimized.setDisable(false);
		menuOLCM.setDisable(true);
		menuPositioning.setDisable(true);
//		menuItemPositioning.setDisable(false);
//		menuItemleftupper.setDisable(false);
//		menuItemleftlower.setDisable(false);
//		menuItemrightlower.setDisable(false);
//		menuItemrightupper.setDisable(false);
//		menuItemShowDummyNodes.setDisable(false);
	}
	
	/**
	 * Clears the lists that were created for drawing the graph. This is called each time the 
	 * visualization method is changed.
	 */
	private void clearLists() {
		edgeLines.clear();
		nodeCircles.clear();
		labels.clear();
		dummyLabels.clear();
		dummyNodeCircles.clear();
		sourceEdges.clear();
		zoomGroup.getChildren().clear();
		menuItemStraightenEdges.setSelected(false);
    	menuItemStraightenEdges.setDisable(true);
    	menuItemShowLabels.setSelected(false);
    	menuItemShowEdges.setSelected(true);
	}

	/**
	 * Draws the graph straight forward by positioning the nodes on a grid and drawing the edges between them.
	 * This is used for the initial drawing of the graph without any optimization or following any design 
	 * guideline.
	 * Use better drawing methods later ;)
	 * @param graph The graph to draw.
	 * @return The Pane that the graph was drawn on.
	 */
	private AnchorPane drawGraphInitial(Graph graph) {
		clearLists();
		AnchorPane pane = new AnchorPane();
		//draw nodes
		drawNodesOnGrid(graph, pane);
		//draw edges
		for(Edge e : graph.getEdges()) {
			Node source = e.getStart();
			Node target = e.getEnd();
//			System.out.println(graph.getNodes().indexOf(source) + " to " + graph.getNodes().indexOf(target));
			Arrow edgeLine = new Arrow(new Line(nodeCircles.get(graph.getNodes().indexOf(source)).getCenterX(),
					nodeCircles.get(graph.getNodes().indexOf(source)).getCenterY(),
					nodeCircles.get(graph.getNodes().indexOf(target)).getCenterX(),
					nodeCircles.get(graph.getNodes().indexOf(target)).getCenterY()));
			edgeLine.setFill(pref.getEdgeColor());
			edgeLine.setStrokeWidth(pref.getEdgeStrokeWidth());
			edgeLines.add(edgeLine);
			pane.getChildren().add(edgeLine);
		} 
		lbGraphInfo.setText("Graph: " + edgeType + "; unoptimized");
		lbNodesEdges.setText(nodeCircles.size() + " nodes / " + edgeLines.size() + " edges");
		return pane;
	}

	/**
	 * Draws the nodes of the given graph as circles on a defined grid.
	 * @param graph The graph to draw.
	 * @param pane The Pane to draw on.
	 */
	private void drawNodesOnGrid(Graph graph, AnchorPane pane) {
		int clusterHeight = (int) Math.ceil(Math.sqrt((double)(graph.getNodes().size())));
		int clusterWidth = clusterHeight;
		for(int i = 0; i < clusterHeight; i++) {
			for(int j = 0; j < clusterWidth; j++) {
				if((i * j) < graph.getNodes().size() - 1) {
					Circle c = new Circle(i * pref.getGapBetweenNodes() + (pref.getNodeRadius() * 2),
							j * pref.getGapBetweenLayers() + (pref.getNodeRadius() * 2), pref.getNodeRadius());
					c.setFill(pref.getNodeColor());
					nodeCircles.add(c);
					int index = nodeCircles.indexOf(c);
					if(index < graph.getNodes().size() && index >= 0) {
						Tooltip t = new Tooltip(graph.getNodes().get(index).getName());
						Tooltip.install(c, t);
						c.addEventFilter(MouseEvent.MOUSE_CLICKED, e -> {
							//TODO change to output in UI or to something that is needed^^
							System.out.println("Outgoing edges to: ");
							for(Edge edge : graph.getNodes().get(index).getEdgesOut()) {
								System.out.println(edge.getEnd().getName());
							}
						});
						Text label = new Text(c.getCenterX(), c.getCenterY(), t.getText());
						labels.add(label);
						pane.getChildren().add(c);
//						System.out.println(graph.getNodes().get(index).getName());
					}
				}
			}
		}
	}

	/**
	 * Shows a popup window to let the user choose from the available types of edges.
	 */
	private void showTypePopup() {
		try {
			// Load root layout from fxml file.
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("ui/TypePopup.fxml"));
			BorderPane rootLayout = (BorderPane) loader.load();
			// Give the controller access to the main app.
			TypePopupController controller = loader.getController();
			controller.setMainPanelController(this);

			// Show the scene containing the root layout.
			Scene scene = new Scene(rootLayout);
			scene.getStylesheets().add(MainApp.class.getResource("application.css").toString());
			Stage popupStage = new Stage();
			popupStage.setScene(scene);
			popupStage.setResizable(false);
			popupStage.setTitle("Choose edge type...");
			controller.setStage(popupStage);
			popupStage.showAndWait();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Sets the edge type.
	 * @param edgeType The type to set.
	 */
	public void setEdgeType(String edgeType) {
		this.edgeType = edgeType;
	}
	
	/**
	 * Assigns layers to the nodes of the graph and transforms it from the naive drawn graph
	 * to a more reasonable one. This is the foundation of the next step, the cross minimalization.
	 * @param event
	 */
    @FXML
    void handleMenuItemAssignLayer(ActionEvent event) {
    	clearLists();
    	Graph graph = xmlImport.parseToGraph(edgeType);
    	dag = new GreedyAcyclicGraph(graph);
    	//init layer assignment
    	layerAss = new LayerAssignment(dag);
    	dummynodes = dag.getDummyNodes().size();
    	origEdges = dag.getOriginalDummyEdges(graph.getEdges());
    	zoomGroup.getChildren().add(drawAssignedLayers(layerAss.getLayers()));
    	lbGraphInfo.setText("Graph: " + edgeType + "; assigned layers");
    	lbNodesEdges.setText((nodeCircles.size() + dummyNodeCircles.size()) + " nodes (" + dummynodes +
    			" dummy nodes) / " + edgeLines.size() + " edges");
    	menuItemShowDummyNodes.setDisable(false);
    	menuOLCM.setDisable(false);
    	menuPositioning.setDisable(false);
    	menuItemPositioning.setDisable(false);
    	//menuItemStraightenEdges.setSelected(false);
    	//menuItemStraightenEdges.setDisable(true);
    }
	
    /**
     * Draws the graph according to the assigned layers. So the y-coordinate of the nodes are computed
     * by the layer assignment and the x-coordinate is chosen by occurance. In the next step the x-coordinate
     * will be computed.
     * @param layers The acyclic graph to with nodes assigned to layers. 
     */
	private AnchorPane drawAssignedLayers(Map<Integer, LinkedList<Node>> layers) {
		AnchorPane pane = new AnchorPane();
		//draw nodes according to layer assignment
		for (int yPos : layers.keySet()){
			for (Node node : layers.get(yPos)){
				Circle c = new Circle(node.getX_coord() * pref.getGapBetweenNodes() + (pref.getNodeRadius() * 2), 
						node.getY_coord() * pref.getGapBetweenLayers() + (pref.getNodeRadius() * 2), pref.getNodeRadius());
				c.setFill(pref.getNodeColor());
				if(node instanceof DummyNode) {
					dummyNodeCircles.add(c);
				} else  {
					nodeCircles.add(c);
				}
				//add some fancy tooltips
				Tooltip t = new Tooltip(node.getName());
				Tooltip.install(c, t);
				//do something if clicked on the node circle
				c.addEventFilter(MouseEvent.MOUSE_CLICKED, e -> {
					//TODO change to output in UI or to something that is needed^^
					System.out.println("\n\n" + node.getName() + "\nOutgoing edges to: ");
					for(Edge edge : node.getEdgesOut()) {
						System.out.println(edge.getEnd().getName());
					}
					System.out.println("Incoming edges from: ");
					for(Edge edge : node.getEdgesIn()) {
						System.out.println(edge.getStart().getName());
					}
				});
				Text label = new Text(c.getCenterX(), c.getCenterY(), t.getText());
				if(node instanceof DummyNode) {
					dummyLabels.add(label);
				} else {
					labels.add(label);
				}
				pane.getChildren().add(c);
				
				//draw edges
				for(Edge e : node.getEdgesOut()) {
					Node source = e.getStart();
					Node target = e.getEnd();
					Arrow edgeLine = new Arrow(new Line(
							source.getX_coord() * pref.getGapBetweenNodes() + (pref.getNodeRadius() * 2),
							source.getY_coord() * pref.getGapBetweenLayers() + (pref.getNodeRadius() * 2),
							target.getX_coord() * pref.getGapBetweenNodes() + (pref.getNodeRadius() * 2),
							target.getY_coord() * pref.getGapBetweenLayers() + (pref.getNodeRadius() * 2)
					));
					edgeLine.setIsDummyEdge(target instanceof DummyNode);
					edgeLine.setFill(pref.getEdgeColor());
					edgeLine.setStrokeWidth(pref.getEdgeStrokeWidth());
					edgeLines.add(edgeLine);
				}
			}
		}
		pane.getChildren().addAll(edgeLines);
		return pane;
	}
	
	
	/**
	 * draws original edges
	 */
	@FXML
    void handleMenuItemDrawOriginalEdges(){
		AnchorPane content = ((AnchorPane)zoomGroup.getChildren().get(0));
		content.getChildren().removeAll(edgeLines);
		//List<Arrow> 
		edgeLines = new LinkedList<>();
		
		for(Edge e : origEdges) {
			Node source = e.getStart();
			Node target = e.getEnd();
			Arrow edgeLine = new Arrow(new Line(
					source.getX_coord() * pref.getGapBetweenNodes() + (pref.getNodeRadius() * 2),
					source.getY_coord() * pref.getGapBetweenLayers() + (pref.getNodeRadius() * 2),
					target.getX_coord() * pref.getGapBetweenNodes() + (pref.getNodeRadius() * 2),
					target.getY_coord() * pref.getGapBetweenLayers() + (pref.getNodeRadius() * 2)));
			edgeLine.setIsDummyEdge(target instanceof DummyNode);
			edgeLine.setFill(pref.getEdgeColor());
			edgeLine.setStrokeWidth(pref.getEdgeStrokeWidth());
			edgeLines.add(edgeLine);
			//pane.getChildren().add(edgeLine);
		}
		
		content.getChildren().addAll(edgeLines);
	}

	/**
	 * Transforms the graph in an acyclic directed graph (DAG) by removing all cycles.
	 * @param event
	 */
    @FXML
    void handleMenuItemRemoveCycles(ActionEvent event) {
    	clearLists();
    	
    	//draw graph
    	zoomGroup.getChildren().add(drawAcyclicGraph(new GreedyAcyclicGraph(xmlImport.parseToGraph(edgeType))));
    	lbGraphInfo.setText("Graph: " + edgeType + "; unoptimized, acyclic");
    	lbNodesEdges.setText((nodeCircles.size() + dummyNodeCircles.size()) + " nodes / " + edgeLines.size() + " edges");
    	
    }
	
    /**
     * Draws the acyclic graph on a defined grid.
     * @param graph The acyclic graph to draw
     * @return The pane with the drawn graph.
     */
	private AnchorPane drawAcyclicGraph(GreedyAcyclicGraph graph) {
		AnchorPane pane = new AnchorPane();
		drawNodesOnGrid(graph, pane);
		//TODO add option to highlight start nodes
		//draw edges
		for(Edge e : graph.getEdges()) {
			Node source = e.getStart();
			Node target = e.getEnd();
			Arrow edgeLine = new Arrow(new Line(nodeCircles.get(graph.getNodes().indexOf(source)).getCenterX(),
					nodeCircles.get(graph.getNodes().indexOf(source)).getCenterY(),
					nodeCircles.get(graph.getNodes().indexOf(target)).getCenterX(),
					nodeCircles.get(graph.getNodes().indexOf(target)).getCenterY()));
			edgeLine.setFill(pref.getEdgeColor());
			edgeLine.setStrokeWidth(pref.getEdgeStrokeWidth());
			edgeLines.add(edgeLine);
			pane.getChildren().add(edgeLine);
		}
		return pane;
	}
	
	/**
	 * Calls the barycenter heuristic for one layer cross minimalization to reduce the crossings of 
	 * edges in the graph between neighboring layers.
	 * @param event
	 */
    @FXML
    void handleMenuItemOLCMBarycenter(ActionEvent event) {
    	clearLists();

    	Map<Integer, LinkedList<Node>> layers = layerAss.getLayers();
    	for(int i = 0; i < pref.getOlcmRuns(); i++) {
    		layers = OneLayerCrossMinimization.barycenterOLCM(layers);
    	}
    	layerAss.setLayers(layers);
    	//draw graph
    	zoomGroup.getChildren().add(drawAssignedLayers(layerAss.getLayers()));
    	lbGraphInfo.setText("Graph: " + edgeType + "; optimized, OLCM: Barycenter");
    	lbNodesEdges.setText((nodeCircles.size() + dummyNodeCircles.size()) + " nodes (" + dummynodes +
    			" dummy nodes) / " + edgeLines.size() + " edges");
    	menuItemShowDummyNodes.setDisable(false);
    	menuPositioning.setDisable(false);
    	menuItemPositioning.setDisable(false);
		
    }
    
    @FXML
    void handleMenuItemMLCM(ActionEvent event) {
    	clearLists();

    	
    	MultiLayerCrossMinimization mlcm = new MultiLayerCrossMinimization(layerAss, dag, pref.getMlcmRuns());

    	Map<Integer, LinkedList<Node>> mlcmlayers = mlcm.globalKLevelCrossReduction();
		layerAss.setLayers(mlcmlayers);
		layerAss.setNodeLayers();

    	//draw graph
    	zoomGroup.getChildren().add(drawAssignedLayers(mlcmlayers));
    	lbGraphInfo.setText("Graph: " + edgeType + "; optimized, MLCM");
    	lbNodesEdges.setText((nodeCircles.size() + dummyNodeCircles.size()) + " nodes (" + dummynodes +
    			" dummy nodes) / " + edgeLines.size() + " edges");
    	menuItemShowDummyNodes.setDisable(false);
    	menuPositioning.setDisable(false);
    	menuItemPositioning.setDisable(false);
    	
    }
    
    /**
     * sets x-coordinates of all vertices (algorithm of brandes and koepf)
     * @param event
     */
    @FXML
    void handleMenuItemPositioning(ActionEvent event) {
    	clearLists();

    	layerAss.setNodeLayers();
    	VertexPositioning vp = new VertexPositioning(layerAss);
    	vp.setxCoord();
    
    	zoomGroup.getChildren().add(drawAssignedLayers(vp.getLayers()));
    	lbGraphInfo.setText("Graph: " + edgeType + "; optimized, Median Positioning");
    	lbNodesEdges.setText((nodeCircles.size() + dummyNodeCircles.size()) + " nodes (" + dummynodes +
    			" dummy nodes) / " + edgeLines.size() + " edges");
    	menuItemShowDummyNodes.setDisable(false);
    	menuItemShowDummyNodes.setSelected(true);
    	menuItemleftupper.setDisable(false);
		menuItemleftlower.setDisable(false);
		menuItemrightlower.setDisable(false);
		menuItemrightupper.setDisable(false);
		
    }
    
    @FXML
    void handleMenuItemleftupperPositioning(ActionEvent event) {
    	clearLists();

    	layerAss.setNodeLayers();
    	VertexPositioning vp = new VertexPositioning(layerAss);
    	vp.setxCoord(0);
    	zoomGroup.getChildren().add(drawAssignedLayers(vp.getLayers()));
    	lbGraphInfo.setText("Graph: " + edgeType + "; optimized, Left Upper Positioning");
    	lbNodesEdges.setText((nodeCircles.size() + dummyNodeCircles.size()) + " nodes (" + dummynodes +
    			" dummy nodes) / " + edgeLines.size() + " edges");
    	menuItemShowDummyNodes.setDisable(false);
    	menuItemStraightenEdges.setDisable(true);
    }
    
    @FXML
    void handleMenuItemleftlowerPositioning(ActionEvent event) {
    	clearLists();
    	
    	layerAss.setNodeLayers();
    	VertexPositioning vp = new VertexPositioning(layerAss);
    	vp.setxCoord(1);
    	zoomGroup.getChildren().add(drawAssignedLayers(vp.getLayers()));
    	lbGraphInfo.setText("Graph: " + edgeType + "; optimized, Left Lower Positioning");
    	lbNodesEdges.setText((nodeCircles.size() + dummyNodeCircles.size()) + " nodes (" + dummynodes +
    			" dummy nodes) / " + edgeLines.size() + " edges");
    	menuItemShowDummyNodes.setDisable(false);
    }
    
    @FXML
    void handleMenuItemrightlowerPositioning(ActionEvent event) {
    	clearLists();
    	
    	layerAss.setNodeLayers();
    	VertexPositioning vp = new VertexPositioning(layerAss);
    	vp.setxCoord(2);
    	zoomGroup.getChildren().add(drawAssignedLayers(vp.getLayers()));
    	lbGraphInfo.setText("Graph: " + edgeType + "; optimized, Right Lower Positioning");
    	lbNodesEdges.setText((nodeCircles.size() + dummyNodeCircles.size()) + " nodes (" + dummynodes +
    			" dummy nodes) / " + edgeLines.size() + " edges");
    	menuItemShowDummyNodes.setDisable(false);
    }
    
    @FXML
    void handleMenuItemrightupperPositioning(ActionEvent event) {
    	clearLists();
    	
    	layerAss.setNodeLayers();
    	VertexPositioning vp = new VertexPositioning(layerAss);
    	vp.setxCoord(3);
    	zoomGroup.getChildren().add(drawAssignedLayers(vp.getLayers()));
    	lbGraphInfo.setText("Graph: " + edgeType + "; optimized, Right Upper Positioning");
    	lbNodesEdges.setText((nodeCircles.size() + dummyNodeCircles.size()) + " nodes (" + dummynodes +
    			" dummy nodes) / " + edgeLines.size() + " edges");
    	menuItemShowDummyNodes.setDisable(false);
    }


	/**
	 * Shows/hides the edges in the GUI.
	 * @param event
	 */
    @FXML
    void handleMenuItemShowEdges(ActionEvent event) {
    	AnchorPane content = ((AnchorPane)zoomGroup.getChildren().get(0));
    	if(menuItemShowEdges.isSelected()) {
    		content.getChildren().addAll(edgeLines);
    	} else {
    		content.getChildren().removeAll(edgeLines);
    	}
    }
    
	/**
	 * Transforms the graph in an acyclic directed graph (DAG) by removing all cycles.
	 * @param event
	 */
    @FXML
    void handleMenuItemShowUnoptimized(ActionEvent event) {
    	clearLists();
    	
    	zoomGroup.getChildren().add(drawGraphInitial(xmlImport.parseToGraph(edgeType)));
    	lbGraphInfo.setText("Graph: " + edgeType + "; unoptimized"); 
    }
    
	/**
	 * Shows/hides the labels in the GUI.
	 * @param event
	 */
    @FXML
    void handleMenuItemShowLabels(ActionEvent event) {
    	AnchorPane content = ((AnchorPane)zoomGroup.getChildren().get(0));
    	if(menuItemShowLabels.isSelected()) {
    		content.getChildren().addAll(labels);
    		if(menuItemShowDummyNodes.isSelected()) {
    			content.getChildren().addAll(dummyLabels);
    		}
    	} else {
    		content.getChildren().removeAll(labels);
    		content.getChildren().removeAll(dummyLabels);
    	}
    }
    
    /**
     * Enables or disables the coloring of dummy nodes in a different color than the other nodes for highlighting.
     * @param event
     */
    @FXML
    void handleMenuItemHighlightDummyNodes(ActionEvent event) {
    	if(menuItemHighlightDummyNodes.isSelected()) {
    		for(Circle c : dummyNodeCircles) {
    			c.setFill(pref.getDummynodeHighlightColor());
    		}
    	} else {
    		for(Circle c : dummyNodeCircles) {
    			c.setFill(pref.getNodeColor());
    		}
    	}
    }
    
    /**
     * Removes dummy nodes.
     * @param event
     */
    @FXML
    void handleMenuItemShowDummyNodes(ActionEvent event) {
		AnchorPane content = ((AnchorPane)zoomGroup.getChildren().get(0));
    	if(menuItemShowDummyNodes.isSelected()) {
    		if(!content.getChildren().containsAll(dummyNodeCircles)) {
    			content.getChildren().addAll(dummyNodeCircles);
        		for(Arrow a : edgeLines) {
        			if(a.isDummyEdge()) {
            			a.showArrowHead();
            		}
        		}
        		content.getChildren().removeAll(edgeLines);
        		content.getChildren().addAll(edgeLines);
        		if(menuItemShowLabels.isSelected()) {
        			content.getChildren().addAll(dummyLabels);
        		}
        		menuItemStraightenEdges.setDisable(true);
        		menuItemStraightenEdges.setSelected(false);
        		content.getChildren().removeAll(sourceEdges);
            	lbNodesEdges.setText((nodeCircles.size() + dummyNodeCircles.size()) + " nodes (" + dummyNodeCircles.size() +
            			" dummy nodes) / " + edgeLines.size() + " edges");
    		}
    	} else {
        	content.getChildren().removeAll(dummyNodeCircles);
        	content.getChildren().removeAll(dummyLabels);
        	for(Arrow a : edgeLines) {
        		if(a.isDummyEdge()) {
        			a.hideArrowHead();
        		}
    		}
        	menuItemStraightenEdges.setDisable(false);
    		lbNodesEdges.setText(nodeCircles.size() + " nodes / " + edgeLines.size() + " edges");
    	}
    }
    
    /**
     * Straightens edges be removing kinks and drawing the original edges.
     * @param event
     */
    @FXML
    void handleMenuItemStraightenEdges(ActionEvent event) {
		AnchorPane content = ((AnchorPane)zoomGroup.getChildren().get(0));
    	if(menuItemStraightenEdges.isSelected()) {
        	content.getChildren().removeAll(edgeLines);
        	if(sourceEdges.isEmpty()) {
        		drawOriginalEdges(content);	
        	}
        	content.getChildren().addAll(sourceEdges);
    	} else {
    		content.getChildren().removeAll(sourceEdges);
    		content.getChildren().addAll(edgeLines);
    	}
    }
	
    /**
     * Draws the original edges parsed from the source graph file.
     * @param content
     */
	private void drawOriginalEdges(AnchorPane content) {
		
    	for (Edge e : origEdges){
    		Node n = e.getStart();
    		Node target = e.getEnd();
    		if(!(target instanceof DummyNode)) {
    					
        				//skip dummy nodes until actual node is reached
        				while(n instanceof DummyNode) {

        					for (Edge o: origEdges){
        						if (n instanceof DummyNode && n.equals(o.getEnd())){
        							n = o.getStart();
        				
        						}
        					}
        					
        				}
        				Arrow edgeLine = new Arrow(new Line(
    							n.getX_coord() * pref.getGapBetweenNodes() + (pref.getNodeRadius() * 2),
    							n.getY_coord() * pref.getGapBetweenLayers() + (pref.getNodeRadius() * 2),
    							target.getX_coord() * pref.getGapBetweenNodes() + (pref.getNodeRadius() * 2),
    							target.getY_coord() * pref.getGapBetweenLayers() + (pref.getNodeRadius() * 2)
    					));
    					edgeLine.setFill(pref.getEdgeColor());
    					edgeLine.setStrokeWidth(pref.getEdgeStrokeWidth());
    					sourceEdges.add(edgeLine);
        		
    		}
    	}
	}

	/**
	 * Zooms the graph.
	 * @param percentage
	 */
	private void zoom() {
		zoomScale.setX(zoom);
		zoomScale.setY(zoom);
	}
	
    @FXML
    void handleMenuItemZoom25(ActionEvent event) {
    	zoom = .25;
    	zoom();
    }

    @FXML
    void handleMenuItemZoom50(ActionEvent event) {
    	zoom = .5;
    	zoom();
    }

    @FXML
    void handleMenuItemZoom75(ActionEvent event) {
    	zoom = .75;
    	zoom();
    }
    
    @FXML
    void handleMenuItemZoom100(ActionEvent event) {
    	zoom = 1.d;
    	zoom();
    }

    @FXML
    void handleMenuItemZoom150(ActionEvent event) {
    	zoom = 1.5;
    	zoom();
    }

    @FXML
    void handleMenuItemZoom200(ActionEvent event) {
    	zoom = 2.d;
    	zoom();
    }
    
    @FXML
    void handleMenuItemZoom400(ActionEvent event) {
    	zoom = 4.d;
    	zoom();
    }
    
    /**
     * Generates and shows a warning popup with a custom title, header and message. If there should be no header
     * set this to null.
     * @return A warning popup
     */
    public static void showWarning(String title, String header, String message) {
        Alert alert = new Alert(AlertType.ERROR);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(message);
        DialogPane dialogPane = alert.getDialogPane();
        dialogPane.getStylesheets().add(MainApp.class.getResource("application.css").toExternalForm());
        alert.showAndWait();
    }
}
