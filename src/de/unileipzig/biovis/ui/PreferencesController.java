package de.unileipzig.biovis.ui;

import de.unileipzig.biovis.Preferences;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Slider;
import javafx.scene.control.Tab;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class PreferencesController {
	
	private Stage prefStage;
	
	private MainPanelController mainPanelController;
	
	private Preferences pref;
	
	@FXML
    private Tab tabGraph;

    @FXML
    private Slider sliderNodeRadius;

    @FXML
    private Slider sliderNodeGap;
    
    @FXML
    private Slider sliderLayerGap;

    @FXML
    private Slider sliderEdgeStrokeWidth;

    @FXML
    private ColorPicker colorPickerNodeColor;

    @FXML
    private ColorPicker colorPickerDummyNodeColor;

    @FXML
    private ColorPicker colorPickerEdgeColor;
    
//    @FXML
//    private Slider sliderRunsOLCM;
    
    @FXML
    private Slider sliderRunsMLCM;

    @FXML
    private Button btReset;
    
    @FXML
    private Button btOk;
	
	/**
	 * Sets the stage of the preferences window.
	 * @param stagePref
	 */
	public void setStage(Stage stagePref) {
		prefStage = stagePref;
	}

	/**
	 * Sets the main panel controller instance.
	 * @param mainPanelController
	 */
	public void setMainAppController(MainPanelController mainPanelController) {
		this.mainPanelController = mainPanelController;
	}
	
	public void setPreferences(Preferences pref) {
		this.pref = pref;
		updateValues();
	}
	
	 private void updateValues() {
		colorPickerNodeColor.setValue(pref.getNodeColor());
    	colorPickerEdgeColor.setValue(pref.getEdgeColor());
    	colorPickerDummyNodeColor.setValue(pref.getDummynodeHighlightColor());
    	sliderNodeGap.setValue(pref.getGapBetweenNodes());
    	sliderNodeRadius.setValue(pref.getNodeRadius());
    	sliderLayerGap.setValue(pref.getGapBetweenLayers());
    	sliderEdgeStrokeWidth.setValue(pref.getEdgeStrokeWidth());
//    	sliderRunsOLCM.setValue(pref.getOlcmRuns());
    	sliderRunsMLCM.setValue(pref.getMlcmRuns());
	}

	@FXML
    void handleBtReset(ActionEvent event) {
		 pref.setDefaultValues();
		 updateValues();
	}
	
	@FXML
	void handleBtOk(ActionEvent event) {
		prefStage.close();
		//TODO auto refresh visualization
	}

    @FXML
    void handleColorPickerDummyNodeColor(ActionEvent event) {
    	pref.setDummynodeHighlightColor(colorPickerDummyNodeColor.getValue());
    }

    @FXML
    void handleColorPickerEdgeColor(ActionEvent event) {
    	pref.setEdgeColor(colorPickerEdgeColor.getValue());
    }

    @FXML
    void handleColorPickerNodeColor(ActionEvent event) {
    	pref.setNodeColor(colorPickerNodeColor.getValue());
    }

    @FXML
    void handleSliderNodeGap(MouseEvent event) {
    	pref.setGapBetweenNodes((int)sliderNodeGap.getValue());
    }

    @FXML
    void handleSliderLayerGap(MouseEvent event) {
    	pref.setGapBetweenLayers((int)sliderLayerGap.getValue());
    }
    
    @FXML
    void handleSliderNodeRadius(MouseEvent event) {
    	pref.setNodeRadius((int)sliderNodeRadius.getValue());
    }

    @FXML
    void hanldeSliderEdgeStrokeWidth(MouseEvent event) {
    	pref.setEdgeStrokeWidth(sliderEdgeStrokeWidth.getValue());
    }
    
//    @FXML
//    void handleSliderRunsOLCM(MouseEvent event) {
//    	pref.setOlcmRuns((int)sliderRunsOLCM.getValue());
//    }
    
    @FXML
    void handleSliderRunsMLCM(MouseEvent event) {
    	pref.setMlcmRuns((int)sliderRunsMLCM.getValue());
    }
}
