package de.unileipzig.biovis.ecm;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import de.unileipzig.biovis.graph.Edge;
import de.unileipzig.biovis.graph.Graph;
import de.unileipzig.biovis.graph.GreedyAcyclicGraph;
import de.unileipzig.biovis.graph.Node;
import de.unileipzig.biovis.xmlimport.XMLImporter;

/**
 * Class holding different multiple layer cross minimization (MLCM) algorithms for minimizing the
 * crossings of edges over multiple layers.
 * @author 
 *
 */
public class MultiLayerCrossMinimization {
	/**
	 * Graph reading from data
	 */
	
	
	
	private final List<String> DATA_FILES = Arrays.asList("data/xml/test.graphml","data/xml/Checkstyle-6.5.graphml", "data/xml/JFtp.graphml",
			"data/xml/JUnit-4.12.graphml", "data/xml/Stripes-1.5.7.graphml", "data/xml/test.graphml");
	private XMLImporter xmlImport = new XMLImporter(new File(DATA_FILES.get(0)));
	private Graph parsedGraph = xmlImport.parseToGraph(XMLImporter.EDGE_TYPES.get(2));
	private Map<Node, LinkedList<Node>> neighboursPlus  = new HashMap<Node,LinkedList<Node>>();
	private Map<Node, LinkedList<Node>> neighboursMinus = new HashMap<Node,LinkedList<Node>>();
	private final int RHO;
	
	/**
	 * Graph acyclic and dummy nodes
	 */
	//static GreedyAcyclicGraph dag = new GreedyAcyclicGraph(parsedGraph);
	private GreedyAcyclicGraph dag;
	
	/**
	 * Layerassignment
	 */
	
	private LayerAssignment layerassignment;
	private List<Edge> edges;
	private Map<Integer,LinkedList<Node>> layers;
	//static LayerAssignment layerassignment = new LayerAssignment(dag);
	//static List<Edge> edges = dag.getEdges();
	//static Map<Integer,LinkedList<Node>> layers = layerassignment.getLayers();
	
	
	private Map<Integer, LinkedList<Node>> blocks  = new HashMap<Integer,LinkedList<Node>>();
	private Map<Node, LinkedList<Node>> plusN = new HashMap<Node, LinkedList<Node>>();
	private Map<Node, LinkedList<Node>> minusN = new HashMap<Node, LinkedList<Node>>();
	private Map<Node, LinkedList<Integer>> plusI = new HashMap<Node, LinkedList<Integer>>();
	private Map<Node, LinkedList<Integer>> minusI = new HashMap<Node, LinkedList<Integer>>();
	
	public MultiLayerCrossMinimization(LayerAssignment layerassignment, GreedyAcyclicGraph dag, int runs){
		this.layerassignment = layerassignment;
		this.dag = dag;
		this.edges = dag.getEdges();
		this.layers = layerassignment.getLayers();
		this.RHO = runs;
	}
	
	
	public Map<Integer, LinkedList<Node>> globalKLevelCrossReduction() {
		defineBlocks();
		for(int i = 0; i < RHO; i++){
			for(Entry<Integer, LinkedList<Node>> entry : blocks.entrySet()){
				siftingStep(blocks, entry);
			}
		}
		newLayers();

		return layers;
	}
	
	public void defineBlocks(){
		for(Node node : dag.getNodes()){
			int key = dag.getNodes().indexOf(node);
			LinkedList<Node> list = new LinkedList<Node>();
			list.add(node);
			list.add(node);
			blocks.put(key, list);
		}
		
		for(Node node : dag.getDummyNodes()){
			findUpper(node);
		}
		
	}
	
	public void findUpper(Node node){
		for(Edge edge : edges){
			if(node == edge.getEnd() && dag.getNodes().contains(edge.getStart())){
				Node upper = node;
				LinkedList<Node> list = new LinkedList<Node>();
				list.add(upper);
				findLower(upper, list);
			}
			
		}
	}
	
	public void findLower(Node node, LinkedList<Node> list){
		
		for(Edge edge : edges){
			if(edge.getStart() == node && dag.getNodes().contains(edge.getEnd())){
				Node lower = node;
				list.add(lower);
				int index = blocks.size();
				blocks.put(index, list);
			}
			if(edge.getStart() == node && dag.getDummyNodes().contains(edge.getEnd())){
				Node middleNode = edge.getEnd();
				findLower(middleNode, list);
			}
		}
	}
	
	public LinkedList<Node> findUV(Node v){
		LinkedList<Node> listUV = new LinkedList<Node>();
		for(Edge edge : edges){
			if(v == edge.getEnd()){
				listUV.add(edge.getStart());
			}
		}
		return listUV;
	}
	
	public LinkedList<Node> findWX(Node w){
		LinkedList<Node> listWX = new LinkedList<Node>();
		for(Edge edge : edges){
			if(w == edge.getStart()){
				listWX.add(edge.getEnd());
			}
		}
		return listWX;
	}
	
	public int getBlock(Node node, Map<Integer, LinkedList<Node>> blocks2){
		for(Entry<Integer, LinkedList<Node>> block : blocks2.entrySet()){
			if(block.getValue().contains(node)){
				int pi = block.getKey();
				return pi;
			}
		}
		return -2;
	}
	
	public void setIUV(Node u, Node v){
		int piU = getBlock(u, blocks);
		int piV = getBlock(v, blocks);
		int posV = plusN.get(u).indexOf(v);
		int posU = minusN.get(v).indexOf(u);
		if(piV > piU){
			plusI.get(u).set(posV, posU);
		}
		
		minusI.get(v).set(posU, posV);
	}
	
	public void setIWX(Node w, Node x){
		int piW = getBlock(w, blocks);
		int piX = getBlock(x, blocks);
		
		int posW = minusN.get(x).indexOf(w);
		int posX = plusN.get(w).indexOf(x); 
		
		if(piW > piX){
			minusI.get(x).set(posW, posX);
		}
		
		plusI.get(w).set(posX, posW);
	}
	
	public void sortAdjacencies(Map<Integer, LinkedList<Node>> blockMap){
				
		for(Entry<Integer, LinkedList<Node>> block : blockMap.entrySet()){
			Node v = block.getValue().getFirst();
			Node w = block.getValue().getLast();
			
			LinkedList<Node> listUV = findUV(v);
			LinkedList<Node> listWX = findWX(w);
			
			if(!listUV.isEmpty()){
				minusN.put(v, listUV);
				LinkedList<Integer> list = new LinkedList<Integer>();
				for(Node node : listUV){
					list.add(listUV.indexOf(node));
				}
				minusI.put(v, list);
			}
			
			
			if(!listWX.isEmpty()){
				plusN.put(w, listWX);
				LinkedList<Integer> list = new LinkedList<Integer>();
				for(Node node : listWX){
					list.add(listWX.indexOf(node));
				}
				plusI.put(w, list);
			}
			
		}
		
		for(Entry<Integer, LinkedList<Node>> block : blocks.entrySet()){
			Node v = block.getValue().getFirst();
			Node w = block.getValue().getLast();
			
			LinkedList<Node> listUV = findUV(v);
			LinkedList<Node> listWX = findWX(w);
			
			if(!listUV.isEmpty()){
				for(Node u : listUV){
					setIUV(u,v);
				}
			}
			if(!listWX.isEmpty()){
				for(Node x : listWX){
					setIWX(w, x);
				}
			}
		}
	}
	
	public int getLayer(Node node){
		for(Entry<Integer, LinkedList<Node>> layer : layerassignment.getLayers().entrySet()){
			if(layer.getValue().contains(node)){
				return layer.getKey();
			}
		}
		return -1;
	}
	
	public LinkedList<Integer> getLevelsOfBlock(LinkedList<Node> block){
		int upper = getLayer(block.get(0));
		int lower = getLayer(block.get(1));
		LinkedList<Integer> levels = new LinkedList<Integer>();
		for(int i = upper; i <= lower; i+=1 ){
			levels.add(i);
		}
		return levels;
	}
	
	public int uswap(Node a, Node b, LinkedList<Node> dNa, LinkedList<Node> dNb, Map<Integer, LinkedList<Node>> newOrderedBlock){
		int r = dNa.size();
		int s = dNb.size();
		int c = 0;
		
		if(r == 0 || s == 0){
			return c;
		}
		else{
			for(int i = 0; i < r; i++){
				for(int j = 0; j < s; j++){
					int indexBlockXi = getBlock(dNa.get(i), blocks);
					int indexBlockYj = getBlock(dNb.get(j), blocks);
					
					if( indexBlockXi < indexBlockYj){
						c = c + s - j;
						
					}
					else{
						if( indexBlockXi > indexBlockYj){
							c = c - (r-i);
							
						}
						else{
							c = c + (s - j) - (r - i);
							
							
						}
					}
					
				}
				
				
			}
		}
		
		return c;
	}
	
	public Map<Integer, LinkedList<Node>> swapAandB(int keyA, int keyB, Map<Integer, LinkedList<Node>> newOrderedBlock){
		
		LinkedList<Node> nodesA = newOrderedBlock.get(keyA);
		LinkedList<Node> nodesB = newOrderedBlock.get(keyB);
		newOrderedBlock.put(keyA, nodesB);
		newOrderedBlock.put(keyB, nodesA);
		return newOrderedBlock;
		
	}
	
	public void updateAdjacencies(Node a, Node b, LinkedList<Node> dNa, LinkedList<Integer> dIa, LinkedList<Node> dNb, LinkedList<Integer> dIb, Map<Integer, LinkedList<Node>> newOrderedBlock, int d){
		int r = dNa.size();
		int s = dNb.size();
		
		if(r > 0 && s > 0){
			int i = 0;
			int j = 0;
			while(i < r && j < s){
				int indexBlockX = getBlock(dNa.get(i), blocks);
				int indexBlockY = getBlock(dNb.get(j), blocks);
				
				if(indexBlockX < indexBlockY){
					i += 1;
				}
				else{
					if(indexBlockX > indexBlockY){
						j += 1;
					}
					else{
						Node z = dNa.get(i);
						
						int aI = dIa.indexOf(i);
						int bI = dIb.indexOf(j);
						
						
						LinkedList <Node> minusdNz = new LinkedList<Node>();
						LinkedList <Integer> minusdIz = new LinkedList<Integer>();
						if(d > 0 && minusN.containsKey(z)){
							int indexA = minusN.get(z).indexOf(a);
							int indexB = minusN.get(z).indexOf(b);
							Node keyA = minusN.get(z).get(indexA);
							Node keyB = minusN.get(z).get(indexB);
							int posA = minusI.get(z).get(indexA);
							int posB = minusI.get(z).get(indexB);
							
							minusN.get(z).set(indexA, keyB);
							minusN.get(z).set(indexB, keyA);
							minusI.get(z).set(indexA, posB);
							minusI.get(z).set(indexB, posA);
							
							plusI.get(a).set(i, plusI.get(a).get(i)+1);
							plusI.get(b).set(j, plusI.get(b).get(j)-1);
							
							i +=1;
							j +=1;
							
						}
						else{
							if(plusN.containsKey(z)){
								int indexA = plusN.get(z).indexOf(a);
								int indexB = plusN.get(z).indexOf(b);
								Node keyA = plusN.get(z).get(indexA);
								Node keyB = plusN.get(z).get(indexB);
								int posA = plusI.get(z).get(indexA);
								int posB = plusI.get(z).get(indexB);
								
								plusN.get(z).set(indexA, keyB);
								plusN.get(z).set(indexB, keyA);
								plusI.get(z).set(indexA, posB);
								plusI.get(z).set(indexB, posA);
								
								minusI.get(a).set(i, minusI.get(a).get(i)+1);
								minusI.get(b).set(j, minusI.get(b).get(j)-1);
							
								i +=1;
								j += 1;
								
							}
						}	
						
					}
				}
				
			}
		}
	}
	
	public int siftingSwap(int keyB, Map<Integer, LinkedList<Node>> newOrderedBlock){
		int keyA = keyB - 1;
		
		int layerUpperA = getLayer(newOrderedBlock.get(keyA).get(0));
		int layerLowerA = getLayer(newOrderedBlock.get(keyA).get(1));
		
		
		int layerUpperB = getLayer(newOrderedBlock.get(keyB).get(0));
		int layerLowerB = getLayer(newOrderedBlock.get(keyB).get(1));
		
		LinkedList<Integer> layersA = getLevelsOfBlock(newOrderedBlock.get(keyA));
		LinkedList<Integer> layersB = getLevelsOfBlock(newOrderedBlock.get(keyB));

		LinkedList<LinkedList> L = new LinkedList<LinkedList>();
		
		int delta = 0;
		if(layersB.contains(layerUpperA)){
			LinkedList<Integer> list = new LinkedList<Integer>();
			list.add(layerUpperA);
			list.add(-1);	
			L.add(list);
			
		}
		
		if(layersB.contains(layerLowerA)){
			LinkedList<Integer> list = new LinkedList<Integer>();
			list.add(layerLowerA);
			list.add(1);
			L.add(list);
			
		}
		
		if(layersA.contains(layerUpperB)){
			LinkedList<Integer> list = new LinkedList<Integer>();
			list.add(layerUpperB);
			list.add(-1);
			L.add(list);
		}
		
		if(layersA.contains(layerLowerB)){
			LinkedList<Integer> list = new LinkedList<Integer>();
			list.add(layerLowerB);
			list.add(1);	
			L.add(list);
		}
		
		for(LinkedList<Integer> listEntry : L){
			int l = listEntry.getFirst();
			int d = listEntry.getLast();
			for(Node a : newOrderedBlock.get(keyA)){
				if(getLayer(a)==l){
					for(Node b : newOrderedBlock.get(keyB)){
						if(getLayer(b)==l){
							
							LinkedList<Node> dNa = new LinkedList<Node>();
							LinkedList<Node> dNb = new LinkedList<Node>();
							LinkedList<Integer> dIa = new LinkedList<Integer>();
							LinkedList<Integer> dIb = new LinkedList<Integer>();
							
							if(d > 0){
								if(plusN.containsKey(a) && plusN.containsKey(b) ){
									dNa = plusN.get(a);
									dNb = plusN.get(b);
									dIa = plusI.get(a);
									dIb = plusI.get(b);
								}	
							}
							else{
								if(minusN.containsKey(a) && minusN.containsKey(b)){
									dNa = minusN.get(a);
									dNb = minusN.get(b);
									dIa = minusI.get(a);
									dIb = minusI.get(b);
								}	
							}
							
							delta += uswap(a, b, dNa, dNb, newOrderedBlock);
							updateAdjacencies(a, b, dNa, dIa, dNb, dIb, newOrderedBlock, d);
							
						}
					}
				}
			}
			
		}
		
		
		return delta; 
	}
	
	public void siftingStep(Map<Integer, LinkedList<Node>> blockMap, Entry<Integer, LinkedList<Node>> A){
		
		Map<Integer, LinkedList<Node>> newOrderedBlock = new HashMap<Integer, LinkedList<Node>>();
		Map<Integer, LinkedList<Node>> tempBlockMap = new HashMap<Integer, LinkedList<Node>>();
		
		tempBlockMap.putAll(blockMap);
		newOrderedBlock.put(0, A.getValue());
		tempBlockMap.remove(A.getKey());
		int cnt = 1;
		for(Entry<Integer, LinkedList<Node>> tempEntry : tempBlockMap.entrySet()){
			newOrderedBlock.put(cnt, tempEntry.getValue());
			cnt+=1;
		}
		tempBlockMap.putAll(newOrderedBlock);
		sortAdjacencies(newOrderedBlock);	
		
		int chi = 0;
		int tempChi = 0;
		int tempP = 0;
		for(int p = 1; p < newOrderedBlock.size(); p+=1 ){
			chi += siftingSwap(p, newOrderedBlock);
			
			int keyB = p;
			int keyA = keyB-1;	
			newOrderedBlock = swapAandB(keyA, keyB, newOrderedBlock);
		
			if(chi < tempChi){
				tempChi = chi;
				tempP = p;
			}
			//System.out.println(tempP);
		}
		//System.out.println(blocks);
		//System.out.println(tempBlockMap);
		tempBlockMap = blocks;
		newOrderedBlock = newBlockList(tempBlockMap, tempP);
		blocks = newOrderedBlock;
		
	}
	

	private Map<Integer, LinkedList<Node>> newBlockList(Map<Integer, LinkedList<Node>> newOrderedBlock,
			int p) {
		
		if(p != 0){
			LinkedList<Node> tempList = newOrderedBlock.get(0);
			
			for(int i = 0; i < p; i++){
				newOrderedBlock.put(i, newOrderedBlock.get(i+1));
			}
			
			newOrderedBlock.put(p, tempList);
			
		}
		
		return newOrderedBlock;
	}
	
	
	

	public void newLayers(){
		int blockNode;
		LinkedList<Integer> blockList = new LinkedList<Integer>();
		Map<Integer, Node> nodeBlockMap = new HashMap<Integer, Node>();
		Map<Integer, LinkedList<Node>> newLayers = new HashMap<Integer, LinkedList<Node>>();

		for(Entry<Integer, LinkedList<Node>> entry : layers.entrySet()){
			for(Node node : entry.getValue()){
				blockNode = getBlock(node, blocks);
				if(blockNode < 0 ){
					blockNode = middleDummyBlock(node);
				}
				blockList.add(blockNode);
				nodeBlockMap.put(blockNode, node);
			}
			
			blockList.sort(null);
			LinkedList<Node> nodeList = new LinkedList<Node>();
			for(int index : blockList){
				nodeList.add(nodeBlockMap.get(index));
			}
			
			newLayers.put(entry.getKey(), nodeList);
			
			nodeBlockMap.clear();
			blockList.clear();
		}
		
		layers = newLayers;
	}
	
	public int middleDummyBlock(Node node){
		int blockIndex = -1; 
		
		for(Edge edge : dag.getEdges()){
			if(edge.getEnd() == node){
				if(getBlock(edge.getStart(), blocks) >= 0){
					 blockIndex = getBlock(edge.getStart(), blocks);
				}
				else{
					middleDummyBlock(edge.getStart());
				}
			}
		}
		
		return blockIndex;
	}
	

}
