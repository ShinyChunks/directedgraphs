package de.unileipzig.biovis.ecm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import de.unileipzig.biovis.graph.DummyNode;
import de.unileipzig.biovis.graph.Edge;
import de.unileipzig.biovis.graph.Graph;
import de.unileipzig.biovis.graph.GreedyAcyclicGraph;
import de.unileipzig.biovis.graph.Node;

public class LayerAssignment {
	
	
	private LinkedList<String> layerList = new LinkedList<String>();
	private LinkedList<Node> nodesWithLayer = new LinkedList<Node>();
	private ArrayList<Integer> layerIndex = new ArrayList<>();
	private Map<Integer,LinkedList<Node>> layers;
	private GreedyAcyclicGraph graph;
	
	/**
	 * Constructor.
	 * @param graph
	 */
	public LayerAssignment(GreedyAcyclicGraph graph) {
		this.graph = graph;
		assignLayer(graph);
	}

	public void assignLayer(GreedyAcyclicGraph graph){
		List<Node> graphNodes = graph.getNodes();
		int numNodes = graph.getNodes().size();
		layerList = initLayers(graph, numNodes);
		LinkedList<Integer> differences = new LinkedList<Integer>();
		while(!nodesWithLayer.containsAll(graphNodes)){
//			System.out.println(nodesWithLayer.size() + " : " + graphNodes.size());
			differences.add(graphNodes.size() - nodesWithLayer.size());
			for(Node node : graphNodes){
				
				if(!nodesWithLayer.contains(node)){
//					System.out.println(node.getName());
					calculateLayer(node);
				}
			}
		}
//		System.out.println(layers.keySet());
		shiftLayerKeys();
//		System.out.println(layers.keySet());
		setNodeLayers();
		setDummyNodes();

		setNodeLayers();

		//System.out.println(nodesWithLayer.get(nodesWithLayer.size()-1));
		}


		
	public LinkedList<String> initLayers(Graph graph, int numNodes){
			//System.out.println("initLayer");
			String edgeLayer;
			layers = new HashMap<Integer,LinkedList<Node>>();
			layers.put(numNodes, new LinkedList<Node>());
			
			for(Node node: graph.getNodes()){
				List<Edge> children = node.getEdgesOut();
				String nodeName = node.getName();
				
				if(children.isEmpty()){
					layerIndex.add(numNodes);
					nodesWithLayer.add(node);
					edgeLayer = nodeName + " : " + Integer.toString(numNodes);
					layers.get(numNodes).add(node);
				}
			else{
				edgeLayer = nodeName;
			}
			layerList.add(edgeLayer);
			
		}
		return layerList;
	}
		
	public void calculateLayer(Node node){
			
			LinkedList<Node> children = node.getChildren();
			
			if(nodesWithLayer.containsAll(children)){
				ArrayList<Integer> layers = new ArrayList<>();
				 
				
				for(Node child : children){
					int index = nodesWithLayer.indexOf(child);
					layers.add(layerIndex.get(index));
				}
				int min = (int) Collections.min(layers);
				nodesWithLayer.add(node);
				layerIndex.add(min-1);
				if(!this.layers.keySet().contains(min-1)){
					this.layers.put(min-1, new LinkedList<Node>());
				}
				this.layers.get(min-1).add(node);
			}
			

		}
	
	/**
	 * set DummyNodes 
	 */
	
		public void setDummyNodes(){
			LinkedList<Node> layerNodes;
			LinkedList<Node> nextlayerNodes;
			List<Edge> removals;
			List<Edge> edge2dummylist;
			for (int key: layers.keySet()){
				layerNodes = layers.get(key);
				nextlayerNodes = layers.get(key+1);
				int nodeposition;
				for (Node node : layerNodes){
					removals = new LinkedList<Edge>();
					edge2dummylist = new LinkedList<Edge>();
					for (Edge out : node.getEdgesOut()){
						if(!nextlayerNodes.contains(out.getEnd())){
							//add DummyNode
							
							nodeposition = layerNodes.indexOf(node);
//							System.out.println("new Dummynode: "+ nodeposition+ ", layer: "+ (key+1));
							addDummyNode(out, key+1, nodeposition, edge2dummylist);
							removals.add(out);
						}
					}
					node.getEdgesOut().addAll(edge2dummylist);
					removeEdges(removals);
					
				}
			}
		}
		
		/**
		 * remove edges from graph edge list and in and outgoing edges of nodes
		 * @param edges
		 */
		private void removeEdges(List<Edge> edges){
			for (Edge edge : edges){
				edge.remove();
			}
			graph.getEdges().removeAll(edges);
		}
		
		/**
		 * add DummyNode in layer at position between start and end node of edge
		 * @param edge
		 * @param layer
		 * @param position
		 */
		
		private void addDummyNode(Edge edge, int layer, int position, List<Edge> parentedges){
			Node start = edge.getStart();
			Node end = edge.getEnd();
			String name = start.getName();
			if (!start.getClass().equals(DummyNode.class)){
				name = "d_"+name;
			}
			DummyNode dummy = new DummyNode(name);
			Edge in = new Edge(start,dummy);
			Edge out = new Edge(dummy, end);
			parentedges.add(in);
			dummy.addIncomingEdge(in);
			dummy.addOutgoingEdge(out);
			end.addIncomingEdge(out);
			graph.addDummyNode(dummy);
			if(position <= layers.get(layer).size()) {
				layers.get(layer).add(position,dummy);
			} else {
				layers.get(layer).add(dummy);
			}
			
		}
		
		/**
		 * shift Layer keys to start with zero
		 */
	
		private void shiftLayerKeys(){
			int min = Collections.min(layers.keySet());
			Map<Integer,LinkedList<Node>> shiftedLayers = new HashMap<Integer,LinkedList<Node>>();
			for (int key : layers.keySet()){
				shiftedLayers.put(key-min, layers.get(key));
			}
			layers = shiftedLayers;
		}
		
		/**
		 * set coordinates to nodes
		 */
		
		public void setNodeLayers(){
			for (int key : layers.keySet()){
				int cnt = 0;
				for (Node node : layers.get(key)){
					node.setY_coord(key);
					node.setX_coord(cnt);
					cnt++;
				}
			}
		}
		
		/**
		 * Gets the edge crossing matrix between two layers. In each cell there are the number of crossings
		 * between the two corresponding nodes.
		 * @param layer1 The first layer.
		 * @param layer2 The second layer.
		 * @return The edge crossing matrix between the two given layers.
		 */
		public int[][] getEdgeCuttingMatrix(int layer1, int layer2){
			LinkedList<Node> uppernodelayer = layers.get(layer1); 
			LinkedList<Node> lowernodelayer = layers.get(layer2); 
			LinkedList<Node> leftuppernodes; 
			LinkedList<Node> rightuppernodes;
			LinkedList<Node> leftlowernodes;
			LinkedList<Node> rightlowernodes;
			int idx1 = uppernodelayer.size();
			int idx2 = lowernodelayer.size();
			int cnt;
			int[][] ecm = new int [idx1][idx2];
			for (int i = 0; i<idx1; i++){
				
				Node uppernode = uppernodelayer.get(i);
				leftuppernodes = getleftvertices(uppernodelayer, uppernode);
				rightuppernodes = getrightvertices(uppernodelayer, uppernode);
				
				for (int j=0; j<idx2; j++){
					
					cnt = 0;
					Node lowernode = lowernodelayer.get(j);
					if (uppernode.getChildren().contains(lowernode)){
						//System.out.println(uppernode.getName()+"->"+lowernode.getName());
						rightlowernodes = getrightvertices(lowernodelayer, lowernode);
						leftlowernodes = getleftvertices(lowernodelayer, lowernode);

						
						for (Node leftupper: leftuppernodes){
							for (Edge out : leftupper.getEdgesOut()){
								if(rightlowernodes.contains(out.getEnd())){
									cnt++;
								}
							}
						}
						for (Node rightupper: rightuppernodes){
							for (Edge out : rightupper.getEdgesOut()){
								if(leftlowernodes.contains(out.getEnd())){
									cnt++;
								}
							}
						}
					}
					ecm[i][j] = cnt;
					
				}
				
			}
			return ecm;
		}
		
		/**
		 * @param nodelayer
		 * @param node
		 * @return list of all nodes right to node in nodelayer
		 */
		
		public LinkedList<Node> getrightvertices(LinkedList<Node> nodelayer, Node node){
			LinkedList<Node> rightnodes = new LinkedList<Node>();
			boolean addnode = false;
			for (Node right: nodelayer){
				if (addnode)
					rightnodes.add(right);
				if (right.equals(node))
					addnode = true;
				
			}
			if (!addnode)
				System.out.println("node not in list");
			return rightnodes;
			
		}
		/**
		 * 
		 * @param nodelayer
		 * @param node
		 * @return list with all nodes left of node in nodelayer
		 */
		
		public LinkedList<Node> getleftvertices(LinkedList<Node> nodelayer, Node node){
			LinkedList<Node> leftnodes = new LinkedList<Node>();
			for (Node left: nodelayer){
				if (left.equals(node))
					return leftnodes;
				else
					leftnodes.add(left);
			}
			System.out.println("node not in list");
			return leftnodes;
			
		}
			
		
	public Map<Integer, LinkedList<Node>> getLayers() {
			return layers;
	}



	public void setLayers(Map<Integer, LinkedList<Node>> layers) {
		this.layers = layers;
	}



	public LinkedList<String> getLayerList() {
		return layerList;
	}

	public LinkedList<Node> getNodesWithLayer() {
		return nodesWithLayer;
	}

	public ArrayList<Integer> getLayerIndex() {
		return layerIndex;
	}
	
	public GreedyAcyclicGraph getModifiedGraph() {
		return graph;
	}
}
