package de.unileipzig.biovis.ecm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;


import de.unileipzig.biovis.graph.DummyNode;
import de.unileipzig.biovis.graph.Edge;
import de.unileipzig.biovis.graph.Node;

/**
 * Constructor
 */

public class VertexPositioning {
	private LayerAssignment layerassignment;
	private Map<Integer,LinkedList<Node>> layers;
	private Map<Node,Node> root;
	private Map<Node,Node> align;
	private Map<Node,Node> sink;
	private Map<Node,Double> shift  = new HashMap<>();
	private Map<Node,Double> x_pos;
	private Map<Node,List<Double>> xCoords;
	
	public VertexPositioning(LayerAssignment la){
		layerassignment = la;
		layers = layerassignment.getLayers();
		xCoords = initxMap();
		setAlignments();

		
	}

	
	public Map<Integer, LinkedList<Node>> getLayers() {
		return layers;
	}


	public void setAlignments(){
		
		markConflicts(false);
		setLeftUpperAlignment();
		//System.out.println("\n");
		setLeftLowerAlignment();
		//System.out.println("\n");
		clearMarks();
		markConflicts(true);
		setRightLowerAlignment();
		//System.out.println("\n");
		setRightUpperAlignment();
		
		//setxCoord();
	}
	
private void setRightLowerAlignment(){
		
		//System.out.println("\nlower Alignments:");
		setRightLowerVerticalAlignments();
		setRightHorizontalAlignments();
		addxCoords();
		
	}
	
	private void setRightUpperAlignment(){
		
		//System.out.println("\nupper Alignments:");
		setRightUpperVerticalAlignments();
		setRightHorizontalAlignments();
		addxCoords();
		
	}
	
	private void setLeftLowerAlignment(){
		
		//System.out.println("\nlower Alignments:");
		setLeftLowerVerticalAlignments();
		setLeftHorizontalAlignments();
		addxCoords();
		
	}
	
	private void setLeftUpperAlignment(){
		
		//System.out.println("\nupper Alignments:");
		setLeftUpperVerticalAlignments();
		setLeftHorizontalAlignments();
		addxCoords();
		
	}
	
	/**
	 * calculates x-coordinates of nodes 
	 * (right)
	 */
	private void setRightHorizontalAlignments(){
		sink = initNodeMap();
		shift = initShiftMap(false);
		x_pos = new HashMap<>();
		for (Node key : sink.keySet()){
			if(root.get(key).equals(key) && !x_pos.containsKey(key)){
				
				placeBlockRight(key);
			}
		}
		
		for (Node key : sink.keySet()){
			x_pos.put(key, x_pos.get(root.get(key)));
			
			if (shift.get(sink.get(root.get(key)))> Double.NEGATIVE_INFINITY ){// && sink.get(key).equals(key)){
				//System.out.println("node: "+ key.getName()+" x: "+x_pos.get(key)+ " shift: "+shift.get(sink.get(root.get(key))));
				double value = x_pos.get(key)-shift.get(sink.get(root.get(key)));
				//System.out.println("node: "+ key.getName()+" x: "+x_pos.get(key)+ " new: "+ value);
				x_pos.put(key, value);
				
			}
			
			//System.out.println("node: "+ key.getName()+" x: "+x_pos.get(key)+ " shift: "+ shift.get(sink.get(root.get(key))));
			//System.out.println("sink: "+ sink.get(key) + " root: "+ root.get(key));
			
		}
		
		
	}
	
	/**
	 * get max. number of nodes in layers
	 * @return
	 */
	private int getMaxNodes(){
		int max = 0;
		for (List<Node> nodes : layers.values()){
			if (nodes.size()>max){
				max = nodes.size();
			}
		}
		return max;
	}
	
	/**
	 * (right) block placement
	 * @param node
	 */
	private void placeBlockRight(Node node){
		
		Node w = node;
		Node u = null;
		int delta = 1;
		int idx;
		int layer;
		int max = getMaxNodes();
		x_pos.put(node, 1.0 * max);
		do{
			layer = w.getY_coord();
			if (w.getX_coord()<layers.get(layer).size()-1){
				
				idx =layers.get(layer).indexOf(w);
				u = root.get(layers.get(layer).get(idx+1));
				if(!x_pos.containsKey(u)){
					placeBlockRight(u);
				}
				if(sink.get(node).equals(node)){;
					sink.put(node, sink.get(u));
				}
				if (!sink.get(node).equals(sink.get(u))){
					
					double value = Collections.max(Arrays.asList(shift.get(sink.get(u)),x_pos.get(node)-x_pos.get(u)-delta));
					shift.put(sink.get(u),value);
					
				}
				else{
					x_pos.put(node, Collections.min(Arrays.asList(x_pos.get(node),x_pos.get(u)-delta)));
				}
			}
			
			w = align.get(w);
		}while(!node.equals(w));
	}
	
	
	/**
	 * calculates x-coordinates of nodes 
	 * (left)
	 */
	private void setLeftHorizontalAlignments(){
		sink = initNodeMap();
		shift = initShiftMap(true);
		x_pos = new HashMap<>();
		for (Node key : sink.keySet()){
			if(root.get(key).equals(key) && !x_pos.containsKey(key)){
				placeBlockLeft(key);
			}
		}
		
		for (Node key : sink.keySet()){
			x_pos.put(key, x_pos.get(root.get(key)));
			
			if (shift.get(root.get(key))< Double.POSITIVE_INFINITY){
				double value = x_pos.get(key)+shift.get(sink.get(root.get(key)));
				x_pos.put(key, value);
				
			}

		}
		
	}
	/**
	 * (left) block placement
	 * @param node
	 */
	private void placeBlockLeft(Node node){
		x_pos.put(node, 0.0);
		Node w = node;
		Node u = null;
		int delta = 1;
		int idx;
		int layer;
		do{
			if (w.getX_coord()>0){;
				layer = w.getY_coord();
				idx =layers.get(layer).indexOf(w);
				u = root.get(layers.get(layer).get(idx-1));
				if(!x_pos.containsKey(u)){
					placeBlockLeft(u);
				}
				if(sink.get(node).equals(node)){
					sink.put(node, sink.get(u));
				}
				if (!sink.get(node).equals(sink.get(u))){
					double value = Collections.min(Arrays.asList(shift.get(sink.get(u)),x_pos.get(node)-x_pos.get(u)-delta));
					shift.put(sink.get(u),value);
				}
				else{
					x_pos.put(node, Collections.max(Arrays.asList(x_pos.get(node),x_pos.get(u)+delta)));
				}
			}

			w = align.get(w);
		}while(!node.equals(w));
	}
	
	/**
	 * set vertical alignments (blöcke)
	 * upper right
	 */
	private void setRightUpperVerticalAlignments(){
		Edge[] m_edges;
		Edge edge;
		Node u_m = null;
		align = initNodeMap();
		root = initNodeMap();
		Node node;
		double r;
		for (LinkedList<Node> nodes : layers.values()){
			r = getMaxNodes();
			for (int i= nodes.size()-1; i>=0; i--){
				node = nodes.get(i);
				if (!node.getEdgesIn().isEmpty()){
					if (align.get(node).equals(node)){
						m_edges = getMedian(node.getEdgesIn(), true);
						if (m_edges[1]!=null ) edge = m_edges[1];
						else edge = m_edges[0];
						u_m = edge.getStart();
						
						if(!edge.isMarked() && r > u_m.getX_coord()){
							align.put(u_m, node);
							root.put(node, root.get(u_m));
							align.put(node, root.get(node));
							r=u_m.getX_coord();
						}
						
						else if (m_edges[0]!=null && !m_edges[0].isMarked()){
							u_m =m_edges[0].getStart();
							if(!m_edges[0].isMarked() && r > u_m.getX_coord()){
								align.put(u_m, node);
								root.put(node, root.get(u_m));
								align.put(node, root.get(node));
								r=u_m.getX_coord();
							}
						}
					}
				}
			}
			r=nodes.size();
		}
		/*
		for (Node key : root.keySet()){
			
			System.out.println("root: "+key.getName()+": "+root.get(key));
			System.out.println("align: "+key.getName()+": "+align.get(key));
		}//*/
		
	}
	
	/**
	 * set vertical alignments (blöcke)
	 * lower right
	 */
	private void setRightLowerVerticalAlignments(){
		Edge[] m_edges;
		Edge edge;
		Node u_m = null;
		align = initNodeMap();
		root = initNodeMap();
		Node node;
		LinkedList<Node> nodes;
		double r;
		for (int j =0; j< layers.size(); j++){
			nodes = layers.get(layers.size()-1-j);
			r = getMaxNodes();
			for (int i= 0; i<nodes.size(); i++){
				node = nodes.get(nodes.size()-1-i);
				if (!node.getEdgesOut().isEmpty()){
					if (align.get(node).equals(node)){
						m_edges = getMedian(node.getEdgesOut(), false);
						if (m_edges[1]!=null ) edge = m_edges[1];
						else edge = m_edges[0];
						u_m = edge.getEnd();

						if(!edge.isMarked() && r > u_m.getX_coord()){
							//System.out.println("post: "+u_m.getName());
							align.put(u_m, node);
							root.put(node, root.get(u_m));
							align.put(node, root.get(node));
							
							r=u_m.getX_coord();
						}
						
						else if (m_edges[0]!=null && !m_edges[0].isMarked()){
							u_m =m_edges[0].getStart();
							//System.out.println("pre: "+u_m.getName());
							if(!m_edges[0].isMarked() && r > u_m.getX_coord()){
								align.put(u_m, node);
								root.put(node, root.get(u_m));
								align.put(node, root.get(node));
								
								r=u_m.getX_coord();
							}
						}
					}
				}
			}
		}
		/*
		for (Node key : root.keySet()){
			/*if (align.get(key).equals(key)){
				align.put(key, root.get(key));
			}
			System.out.println("root: "+key.getName()+": "+root.get(key));
			System.out.println("align: "+key.getName()+": "+align.get(key));
		}//*/
		
	}
	
	/**
	 * set vertical alignments (blöcke)
	 * lower left
	 */
	
	private void setLeftLowerVerticalAlignments(){
		Edge[] m_edges;
		Node u_m = null;
		align = initNodeMap();
		root = initNodeMap();
		LinkedList<Node> nodes;
		double r;
		for (int i =0; i< layers.size(); i++){
			nodes = layers.get(layers.size()-1-i);
			//System.out.println(nodes.size());
			r=-1;
			for (Node node : nodes){
				//System.out.println(node.getEdgesOut().size());
				if (!node.getEdgesOut().isEmpty()){
					if (align.get(node).equals(node)){
						m_edges = getMedian(node.getEdgesOut(), false);
						u_m = m_edges[0].getEnd();

						//System.out.println(node.getName() + "->" +u_m.getName());
						if(!m_edges[0].isMarked() && r < u_m.getX_coord()){
							align.put(u_m, node);
							root.put(node, root.get(u_m));
							align.put(node, root.get(node));
							
							r=u_m.getX_coord();
						}
						
						else if (m_edges[1]!=null && !m_edges[1].isMarked()){
							u_m =m_edges[1].getEnd();
							if(!m_edges[1].isMarked() && r < u_m.getX_coord()){
								align.put(u_m, node);
								root.put(node, root.get(u_m));
								align.put(node, root.get(node));
								
								r=u_m.getX_coord();
							}
						}
					}
				}
			}
		}
		/*
		for (Node key : root.keySet()){
			
			System.out.println("root: "+key.getName()+": "+root.get(key));
			System.out.println("align: "+key.getName()+": "+align.get(key));
		}	*/	
	}
	/**
	 * set vertical alignments (blöcke)
	 * upper left
	 */
	private void setLeftUpperVerticalAlignments(){
		Edge[] m_edges;
		Node u_m = null;
		align = initNodeMap();
		root = initNodeMap();
		double r;
		for (LinkedList<Node> nodes : layers.values()){
			r=-1;
			for (Node node : nodes){
				if (!node.getEdgesIn().isEmpty()){
					if (align.get(node).equals(node)){
						m_edges = getMedian(node.getEdgesIn(), true);
						u_m = m_edges[0].getStart();

						
						if(!m_edges[0].isMarked() && r < u_m.getX_coord()){
							//System.out.println("pre: "+u_m.getName());
							align.put(u_m, node);
							root.put(node, root.get(u_m));
							align.put(node, root.get(node));
							r=u_m.getX_coord();
						}
						
						else if (m_edges[1]!=null && !m_edges[1].isMarked()){
							u_m =m_edges[1].getStart();
							//System.out.println("pre: "+u_m.getName());
							if(!m_edges[1].isMarked() && r < u_m.getX_coord()){
								align.put(u_m, node);
								root.put(node, root.get(u_m));
								align.put(node, root.get(node));
								r=u_m.getX_coord();
							}
						}
					}
				}
			}
		}
		/*
		for (Node key : root.keySet()){
			System.out.println("root: "+key.getName()+": "+root.get(key));
			System.out.println("align: "+key.getName()+": "+align.get(key));
		}
		//*/
	}
	/**
	 * find median position
	 * @param node
	 * @return array with edges from left and right median node
	 */
	
	private Edge[] getMedian(List<Edge> edges, boolean upper){
		Edge[] m_edges = {null,null};
		Node node;
		double median_left = -1;
		double median_right = -1;
		double x;
		int pos;
		List<Double> positions = new LinkedList<>();
		for (Edge edge : edges){
			if (upper) node = edge.getStart();
			else node= edge.getEnd();
			positions.add(node.getX_coord());
		}
		Collections.sort(positions);
		int len = positions.size();
		if(len%2 ==0){
			pos = (int)(0.5*len-1);
			median_left = positions.get(pos);
			median_right = positions.get(pos+1);
		}
		else{
			pos = (int)(0.5*(len-1));
			median_left = positions.get(pos);
		}
		for(Edge edge : edges){
			if (upper) node = edge.getStart();
			else node= edge.getEnd();
			x = node.getX_coord();
			if(x ==median_left){
				m_edges[0] = edge;
			}
			if (x == median_right){
				m_edges[1] = edge;
			}
		}
		
		return m_edges;
	}
	

	
	private void addxCoords(){
		for (Node node : x_pos.keySet()){
			xCoords.get(node).add(x_pos.get(node));
		}
	}
	
	private Map<Node,List<Double>> initxMap(){
		Map<Node,List<Double>> map = new HashMap<>();
		for (LinkedList<Node> nodes : layers.values()){
			for (Node node : nodes){
				map.put(node, new ArrayList<>());
			}
		}
		return map;
	}
	
	/**
	 * initialize hashmaps of nodes (root and align)
	 * @return
	 */
	private Map<Node,Node> initNodeMap(){
		Map<Node,Node> map = new HashMap<>();
		for (LinkedList<Node> nodes : layers.values()){
			for (Node node : nodes){
				map.put(node, node);
			}
		}
		return map;
	}
	/**
	 * initialize shift HashMap (links between blocks)
	 * @return
	 */
	private Map<Node,Double> initShiftMap(boolean left){
		Map<Node,Double> map = new HashMap<>();
		for (LinkedList<Node> nodes : layers.values()){
			for (Node node : nodes){
				if (left)
					map.put(node, Double.POSITIVE_INFINITY);
				else
					map.put(node, Double.NEGATIVE_INFINITY);
			}
		}
		return map;
	}
	
	/**
	 * marks edges with type conflicts
	 * (preprocessing step for positioning)
	 */
	public void markConflicts(boolean right){
		Node uppernode;
		for (int i=1; i<layers.keySet().size(); i++){
			for (Node lowernode : layers.get(i)){
				if (lowernode.getClass().equals(DummyNode.class)){
					Edge edge = ((DummyNode)lowernode).getEdgeIn();
					uppernode = edge.getStart();
					if (uppernode.getClass().equals(DummyNode.class))
						checkTypeConflict(edge, i,1, right);
					checkTypeConflict(edge, i,2, right);					
				}
				else{
					for (Edge in : lowernode.getEdgesIn()){
						uppernode = in.getStart();
						if (uppernode.getClass().equals(DummyNode.class)){
							//checkTypeConflict(in, i,1, right);
							checkTypeConflict(in, i,2, right);
						}
						else{
							checkTypeConflict(in, i,0, right);
							
						}
					}
				}
			}
			
		}
	}
	
	/**
	 * create node lists of possible crossing edges and marks type conflicts
	 * @param edge
	 * @param i
	 * @param type
	 */

	private void checkTypeConflict(Edge edge, int i, int type, boolean right) {
		Node uppernode = edge.getStart();
		Node lowernode = edge.getEnd();
		LinkedList<Node> leftuppernodes;
		LinkedList<Node> rightuppernodes;
		LinkedList<Node> leftlowernodes;
		LinkedList<Node> rightlowernodes;
		leftuppernodes = layerassignment.getleftvertices(layers.get(i-1), uppernode);
		rightuppernodes = layerassignment.getrightvertices(layers.get(i-1), uppernode);
		rightlowernodes = layerassignment.getrightvertices(layers.get(i), lowernode);
		leftlowernodes = layerassignment.getleftvertices(layers.get(i), lowernode);
		switch(type){
		
		case 0:
			markTypeZeroConflicts(leftuppernodes, rightlowernodes,false,edge, right);
			markTypeZeroConflicts(rightuppernodes, leftlowernodes,true,edge, right);
			break;
		case 1:
			markTypeOneConflicts(leftuppernodes, rightlowernodes);
			markTypeOneConflicts(rightuppernodes, leftlowernodes);
			break;
			
		case 2:
			markTypeTwoConflicts(leftuppernodes, rightlowernodes,false,edge);
			markTypeTwoConflicts(rightuppernodes, leftlowernodes,true,edge);
			break;
		}
	}
	
	/**
	 * search for type 2 conflicts and marks the edges
	 * @param uppernodes
	 * @param lowernodes
	 */
	
	private void markTypeTwoConflicts(LinkedList<Node> uppernodes, LinkedList<Node> lowernodes, boolean left, Edge edge){
		Node lower;
		Edge out;
		for (Node upper: uppernodes){
			if(upper.getClass().equals(DummyNode.class)){
				out = ((DummyNode)upper).getEdgeOut();
				lower = out.getEnd();
				if (lowernodes.contains(lower)){
					if(!left && !out.isMarked() && !edge.isMarked()){
						edge.setMarked(true);
						//System.out.println("type2: "+edge);
						
					}
					else if (left && !out.isMarked()){
						out.setMarked(true);
						//System.out.println("type2: "+out);
						
						
					}
				}
			}
			else{
				for (Edge edgeOut : upper.getEdgesOut()){
					lower = edgeOut.getEnd();
					if(lower.getClass().equals(DummyNode.class)&&lowernodes.contains(lower)){
						if(!left && !edgeOut.isMarked()){
							edge.setMarked(true);
							//System.out.println("type2: "+edge);
						}
						else if (left && !edgeOut.isMarked()){
							edgeOut.setMarked(true);
							
							//System.out.println("type2: "+edgeOut);
						}
						
					}
				}
			}
		}
	}	
	/**
	 * search for type one conflicts and marks the edges (crossing of inner and outer segment)
	 * @param uppernodes
	 * @param lowernodes
	 */
	
	private void markTypeOneConflicts(LinkedList<Node> uppernodes, LinkedList<Node> lowernodes){
		Node lower;
		for (Node upper: uppernodes){
			for (Edge out : upper.getEdgesOut()){
				lower = out.getEnd();
				if(!(upper.getClass().equals(DummyNode.class)&& lower.getClass().equals(DummyNode.class))){
				if(lowernodes.contains(lower) && !out.isMarked()){
					out.setMarked(true);
					//System.out.println("type1: "+out);
				}
			}
			
				
			}
		}
	}
	
	
	/**
	 * search for type 0 conflicts and marks the edges
	 * @param uppernodes
	 * @param lowernodes
	 */
	
	private void markTypeZeroConflicts(LinkedList<Node> uppernodes, LinkedList<Node> lowernodes, boolean left, Edge edge, boolean right){
		Node lower;
		for (Node upper: uppernodes){
			if(!upper.getClass().equals(DummyNode.class)){
				for (Edge out : upper.getEdgesOut()){
					lower = out.getEnd();
					//marks crossing edges
					if(!lower.getClass().equals(DummyNode.class)&&lowernodes.contains(lower)){
						if(!left){
							if (right){
								out.setMarked(true);
								//System.out.println("type0: "+out);
							}
							else{
								edge.setMarked(true);
								//System.out.println("type0: "+edge);
							}
						}
						else if (left ){
							if (right){
								edge.setMarked(true);
								//System.out.println("type0: "+edge);
							}
							else{
								out.setMarked(true);
								//System.out.println("type0: "+out);
							}
						}
						
					}
				/*
					// marks touching edges (incoming)
					if (lower.equals(edge.getEnd())){
						if (right){
							if (left) edge.setMarked(true);
							else out.setMarked(true);
						}
						else{
							if (left) out.setMarked(true);
							else edge.setMarked(true);
						}
					}*/
				}
			}
		}
		//markOutgoingTouchingEdges(edge.getStart(), right);
	}
	
	private void markOutgoingTouchingEdges(Node node,boolean right){
		LinkedList<Edge> edges = (LinkedList<Edge>) node.getEdgesOut();
		if (edges.size()>1){
			for (Edge edge : edges){
				if (!edge.getEnd().getClass().equals(DummyNode.class))
				edge.setMarked(true);
			}
			if (right){
				edges.getLast().setMarked(false);
			}
			else edges.getFirst().setMarked(false);
		}
		
	}
	
	/**
	 * set x-Coordinates to median of calculated values
	 */
	public void setxCoord(){
		List<Double> values;
		double median;
		for (Node node : xCoords.keySet()){
			values = xCoords.get(node);
			//System.out.println("unsorted  "+values.get(0)+" "+values.get(1)+" "+values.get(2)+" "+values.get(3));
			Collections.sort(values);
			median = 0.5*(values.get(1)+values.get(2));
			node.setX_coord(median);	
			//System.out.println(node.getName()+": "+median);
			//System.out.println("sorted  "+values.get(0)+" "+values.get(1)+" "+values.get(2)+" "+values.get(3));
			
		}
	}
	
	public void setxCoord(int i){
		List<Double> values;
		for (Node node : xCoords.keySet()){
			values = xCoords.get(node);
			node.setX_coord(values.get(i));
		}
		
	}

	private void clearMarks(){
		for (Edge edge : layerassignment.getModifiedGraph().getEdges()){
			if (edge.isMarked()){
				edge.setMarked(false);
			}
		}
	}

	


}
