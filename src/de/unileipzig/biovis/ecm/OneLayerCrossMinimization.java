package de.unileipzig.biovis.ecm;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.Map;

import de.unileipzig.biovis.graph.Edge;
import de.unileipzig.biovis.graph.Node;

/**
 * Class holding different one layer cross minimization (OLCM) algorithms for minimizing the
 * crossings of edges on one layer.
 * @author chris
 *
 */
public class OneLayerCrossMinimization {

	/**
	 * The barycenter heuristic for minimizing edge crossings. Like median heuristic but uses average instead 
	 * of the median of the neighbors. This method is the single step between two layers.
	 * @param v1 Constant list of vertices (nodes)
	 * @param v2 Variable list of vertices (nodes)
	 * @return The list v2 sorted descending by average.
	 */
	private static LinkedList<Node> barycenterOLCMTwoLayers(LinkedList<Node> v1, LinkedList<Node> v2) {
		for(Node v : v2) {
			//get the neighbors of the node
			LinkedList<Node> neighbors = new LinkedList<Node>();
			for(Edge e : v.getEdgesIn()) {
				if(v1.contains(e.getStart())) {
					neighbors.add(e.getStart());
				}
			}
			for(Edge e : v.getEdgesOut()) {
				if(v1.contains(e.getEnd())) {
					neighbors.add(e.getEnd());
				}
			}
			//calculate the avg if the node has any neighbors
			if(!neighbors.isEmpty()) {
				//calculate the average of the neighbors on the other layer
				int sumX = 0;
				for(Node n : neighbors) {
					sumX += n.getX_coord();
				}
				v.setX_coord((int)Math.floor(sumX / neighbors.size()));
			}
		}
		//sorts v2 according to the x position
		Collections.sort(v2, new Comparator<Node>() {
			/**
			 *  Returns a negative integer, zero, or a positive integer as the first argument 
			 *  is less than, equal to, or greater than the second.
			 */
			@Override
			public int compare(Node n1, Node n2) {
				int result = 0;
				if(n1.getX_coord() < n2.getX_coord()) {
					result = -1;
				} else if(n1.getX_coord() > n2.getX_coord()) {
					result = 1;
				}
				return result;
			}
		});
		//after sorting the position isn't correct, so change it to ascending order
		for(int i = 0; i < v2.size(); i++) {
			v2.get(i).setX_coord(i);
		}
		return v2;
	}
	
	/**
	 * The barycenter heuristic for minimizing edge crossings. Like median heuristic but uses average instead 
	 * of the median of the neighbors. This method computes the minimized crossings between all permutations.
	 * @param graph The graph to optimize.
	 * @return The optimized graph.
	 */
	public static Map<Integer, LinkedList<Node>> barycenterOLCM(Map<Integer, LinkedList<Node>> layers) {
		for(int i = 0; i < (layers.keySet().size() - 1); i++) {
			LinkedList<Node> newLayer = barycenterOLCMTwoLayers(layers.get(i), layers.get(i+1));
			layers.put(i+1, newLayer);
		}
		return layers;
	}
}
