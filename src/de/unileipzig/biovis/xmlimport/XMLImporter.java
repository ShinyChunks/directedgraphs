package de.unileipzig.biovis.xmlimport;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import de.unileipzig.biovis.graph.Edge;
import de.unileipzig.biovis.graph.Graph;
import de.unileipzig.biovis.graph.GreedyAcyclicGraph;
//import de.unileipzig.biovis.graph.Graph;
import de.unileipzig.biovis.graph.Node;
import de.unileipzig.biovis.graph.Tree;

/**
 * Class for parsing and improting graphml files.
 * @author chris
 *
 */
public class XMLImporter {
	
	/**
	 * The available edge types with their xml ids.
	 * 0 - return-type
	 * 1 - inheritence
	 * 2 - methodcall
	 * 3 - implementation
	 * 4 - parameter
	 * 5 - throws
	 */
	public final static List<String> EDGE_TYPES = Arrays.asList("return-type", "inheritance", "method-call", 
			"implementation", "parameter", "throws");
	
	/**
	 * The xml file to import and parse.
	 */
	private File xmlFile;
	
	/**
	 * The parsed jdom document.
	 */
	private Document xmlDoc;
	
	public XMLImporter(File xmlFileToImport) {
		xmlDoc = null;
		xmlFile = xmlFileToImport;
		//TODO error message if file not exists
	}
	
	/**
	 * Reads a graphml file and parses a JDOM document. The method returns a list with all
	 * nodes of the graph represented by the graphml file.
	 *
	 * @return A list with all child nodes with tag <node>, i.e. a list of all nodes of
	 * the graph.
	 */
	public List<Element> getNodes() {
		return parse("node");
	}
	
	/**
	 * Reads a graphml file and parses a JDOM document. The method returns a list with all
	 * key nodes of the graph represented by the <key> tag in the graphml file.
	 *
	 * @return A list with all child nodes with tag <key>, i.e. a list of all nodes of
	 * the graph.
	 */
	public List<Element> getKeys() {
		return parse("key");
	}
	
	/**
	 * Reads a graphml file and parses a JDOM document. The method returns a list with all
	 * key nodes of the graph represented by the <edge> tag in the graphml file.
	 *
	 * @return A list with all child nodes with tag <edge>, i.e. a list of all edges of
	 * the graph.
	 */
	public List<Element> getEdges() {
		return parse("edge");
	}
	
	/**
	 * Reads a graphml file and parses a JDOM document. The method returns a list with all
	 * xml nodes of the file with the given xml tag.
	 *
	 *@param tag A String representing the tag to extract.
	 * @return A list with all child nodes with tag <tagName>
	 */
	private List<Element> parse(String tag) {
		List<Element> nodes = new LinkedList<Element>();

		try {
	        // initialize document
	        SAXBuilder builder = new SAXBuilder();
	        xmlDoc = builder.build(xmlFile);

	        // The root node of the file should be "smile"
	        Element rootNode = xmlDoc.getRootElement();

	        // Get the children of the root node with the tag <node>
	        // This is the tag holding all nodes of the graph.
	        List<Element> rootChildren = rootNode.getChildren();
	        Element graphNode = null;
	        List<Element> keyNodes = new LinkedList<Element>();
	        for(Element e : rootChildren) {
	        	if(e.getName().equals("graph")) {
	        		graphNode = e;
	        		break;
	        	} else if(e.getName().equals("key")) {
	        		keyNodes.add(e);
	        	}
	        }
	        if(tag.equals("key")) {
	        	nodes = keyNodes;
	        } else if(tag.equals("node")) {
	        	for(Element e : graphNode.getChildren()) {
	        		if(e.getName().equals("node")) {
	        			nodes.add(e);
	        		}
	        	}
	        } else if(tag.equals("edge")) {
	        	for(Element e : graphNode.getChildren()) {
	        		if(e.getName().equals("edge")) {
	        			nodes.add(e);
	        		}
	        	}
	        }
	    } catch (JDOMException e1) {
	        e1.printStackTrace();
	    } catch (IOException e2) {
	        e2.printStackTrace();
	    }
		return nodes;
	}
	
	/**
	 * Parses  the imported graphml file to a graph object.
	 * @return
	 */
	public Graph parseToGraph(String edgeType) {
		LinkedList<Node> nodes = new LinkedList<Node>();
		LinkedList<Edge> edges = new LinkedList<Edge>();
		//get edges from xml
		List<Element> xmlEdges = getEdges();
		//get nodes from xml
		List<Element> xmlNodes = getNodes();
		
		//add all xml nodes as nodes
		for(Element e : xmlNodes) {
			nodes.add(new Node(e.getAttributeValue("id")));
		}
		//add all xml edges
		for(Element e : xmlEdges) {
			List<Element> children = e.getChildren();
			if(children.size() > 1) {
				System.err.println("Error: More then one child. Expected only one child tag called <data>!");
			} else {
				if(children.get(0).getAttributeValue("key").equals(edgeType)) {
					String sourceID = e.getAttributeValue("source");
					String targetID = e.getAttributeValue("target");
					Node source = null, target = null;
					//TODO improve?
					for(Node n : nodes) {
						if(n.getName().equals(sourceID)) {
							source = n;
						} else if(n.getName().equals(targetID)) {
							target = n;
						}
					}
					if(source != null && target != null) {
						Edge edge = new Edge(source, target);
						source.addOutgoingEdge(edge);
						target.addIncomingEdge(edge);
						edges.add(edge);
					}
				}
			}
		}
		return new Graph(edges, nodes);
	}
	
	/**
	 * Parses  the imported graphml file to a tree object.
	 * Node names have to be unique (like java class names).
	 * @return
	 */
	public Tree parseToTree() {
		LinkedList<Node> nodes = new LinkedList<Node>();
		LinkedList<Edge> edges = new LinkedList<Edge>();
		
		//read all graphml-nodes
		List<Element> jdomNodes = parse("node");
		
		//set the first substring before the first dot of the first node as the root
		Node root = new Node(jdomNodes.get(0).getAttributeValue("id").substring(0, 
				jdomNodes.get(0).getAttributeValue("id").indexOf(".")));
		nodes.add(root);
		//add all nodes as children of the root node if the node starts with the name of the 
		//root node, if not we have more then one tree/root
		for(Element e : jdomNodes) {
			String nodeName = e.getAttributeValue("id");
			if(nodeName.startsWith(root.getName())) {
				root.addChildren(new Node(nodeName.substring(nodeName.indexOf(".") + 1)));
			} else {
				//TODO handle case where we have more than one tree
			}
		}
		reorganizeChildren(root, nodes);
		//add edges to edge list
		for(Node n : nodes) {
			edges.addAll(n.getEdgesOut());
		}		

		return new Tree(edges, nodes, root);
	}

	/**
	 * Reorganize the child nodes so that same prefixes are combined to a single node.
	 * @param node The node to reorganize the children for.
	 */
	private void reorganizeChildren(Node node, LinkedList<Node> nodes) {
		//copy children
		LinkedList<Node> childrenCopy = node.getChildren();
		//clear children
		node.clearChildren();
		//search in copy for identical prefixes
		HashSet<String> prefixes = new HashSet<String>();
		for(Node n : childrenCopy) {
			//if the name contains no "." there is nothing to reorganize
			if(n.getName().contains(".")) {
				//get prefix
				String prefix = n.getName().substring(0, n.getName().indexOf("."));
				//if prefix isn't already known add it
				if(!prefixes.contains(prefix)) {
					prefixes.add(prefix);
					Node newNode = new Node(prefix);
					node.addChildren(newNode);
					nodes.add(newNode);
					//add all nodes that start with this prefix as children to the node
					for(Node n2 : childrenCopy) {
						if(n2.getName().startsWith(prefix)) {
							newNode.addChildren(new Node(n2.getName().substring(n2.getName().indexOf(".") + 1)));
						}
					}
					//recursion
					reorganizeChildren(newNode, nodes);
				}
			} else {
				node.addChildren(n);
				nodes.add(n);
			}
		}
	}
	
}