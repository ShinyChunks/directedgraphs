package de.unileipzig.biovis.xmlimport;

import java.util.LinkedList;

public class Path {

	private LinkedList<String> path;
	
	public Path() {
		path = new LinkedList<String>();
	}
	
	/**
	 * Adds a step to the path.
	 * @param step
	 */
	public void addStep(String step) {
		this.path.add(step);
	}
	
	/**
	 * Returns the path.
	 * @return
	 */
	public LinkedList<String> getPath() {
		return this.path;
	}
	
	@Override
	public String toString() {
		String str = "";
		for(String s : path) {
			str += s + " - ";
		}
		//delete last " - "
		str = str.substring(0, str.length() - 3);
		return str;
	}
}
