package de.unileipzig.biovis;

import javafx.scene.paint.Color;

public class Preferences {

	/**
	 * The gap to keep between the nodes while drawing.
	 */
	private final int GAP_BETWEEN_NODES = 60;
	private int gapBetweenNodes;
	
	private final int GAP_BETWEEN_LAYERS = 60;
	private int gapBetweenLayers;
	
	/**
	 * The radius of the node circles.
	 */
	private final int NODE_RADIUS = 20;
	private int nodeRadius;
	
	private final Color DEFAULT_NODE_COLOR = Color.LIGHTGRAY;
	private Color nodeColor;
	
	private final Color DUMMYNODE_HIGHLIGHT_COLOR = Color.LIGHTCORAL;
	private Color dummynodeHighlightColor;
	
	private final Color DEFAULT_EDGE_COLOR = Color.GRAY;
	private Color edgeColor;
	
	private final double DEFAULT_EDGE_STROKE_WIDTH = 1.0;
	private double edgeStrokeWidth;
	
	private final int OLCM_RUNS = 10;
	private int olcmRuns;
	
	private final int MLCM_RUNS = 10;
	private int mlcmRuns;
	
	
	/**
	 * Constructor.
	 */
	public Preferences() {
		setDefaultValues();
	}
	
	public int getGapBetweenNodes() {
		return gapBetweenNodes;
	}

	public void setGapBetweenNodes(int gapBetweenNodes) {
		this.gapBetweenNodes = gapBetweenNodes;
	}

	public int getGapBetweenLayers() {
		return gapBetweenLayers;
	}

	public void setGapBetweenLayers(int gapBetweenLayers) {
		this.gapBetweenLayers = gapBetweenLayers;
	}

	public int getNodeRadius() {
		return nodeRadius;
	}

	public void setNodeRadius(int nodeRadius) {
		this.nodeRadius = nodeRadius;
	}

	public Color getNodeColor() {
		return nodeColor;
	}

	public void setNodeColor(Color nodeColor) {
		this.nodeColor = nodeColor;
	}

	public Color getDummynodeHighlightColor() {
		return dummynodeHighlightColor;
	}

	public void setDummynodeHighlightColor(Color dummynodeHighlightColor) {
		this.dummynodeHighlightColor = dummynodeHighlightColor;
	}

	public Color getEdgeColor() {
		return edgeColor;
	}

	public void setEdgeColor(Color edgeColor) {
		this.edgeColor = edgeColor;
	}

	public double getEdgeStrokeWidth() {
		return edgeStrokeWidth;
	}

	public void setEdgeStrokeWidth(double edgeStrokeWidth) {
		this.edgeStrokeWidth = edgeStrokeWidth;
	}
	
	public int getOlcmRuns() {
		return olcmRuns;
	}

	public void setOlcmRuns(int olcmRuns) {
		this.olcmRuns = olcmRuns;
	}

	public int getMlcmRuns() {
		return mlcmRuns;
	}

	public void setMlcmRuns(int mlcmRuns) {
		this.mlcmRuns = mlcmRuns;
	}

	/**
	 * Sets all values to the default values.
	 */
	public void setDefaultValues() {
		gapBetweenNodes = GAP_BETWEEN_NODES;
		setGapBetweenLayers(GAP_BETWEEN_LAYERS);
		nodeRadius = NODE_RADIUS;
		nodeColor = DEFAULT_NODE_COLOR;
		dummynodeHighlightColor = DUMMYNODE_HIGHLIGHT_COLOR;
		edgeColor = DEFAULT_EDGE_COLOR;
		edgeStrokeWidth = DEFAULT_EDGE_STROKE_WIDTH;
		setOlcmRuns(OLCM_RUNS);
		setMlcmRuns(MLCM_RUNS);
	}
}
