package de.unileipzig.biovis.test;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import de.unileipzig.biovis.graph.Graph;
import de.unileipzig.biovis.graph.GreedyAcyclicGraph;
import de.unileipzig.biovis.xmlimport.XMLImporter;

public class AGTestMain {
	private final static List<String> DATA_FILES = Arrays.asList("data/xml/test.graphml","data/xml/Checkstyle-6.5.graphml", "data/xml/JFtp.graphml",
			"data/xml/JUnit-4.12.graphml", "data/xml/Stripes-1.5.7.graphml");
	
	public static void main(String[] args) {

		XMLImporter xmlImport = new XMLImporter(new File(DATA_FILES.get(0)));
		//Tree parsedTree = xmlImport.parseToTree();
		Graph parsedGraph = xmlImport.parseToGraph(XMLImporter.EDGE_TYPES.get(2));
		
		
		GreedyAcyclicGraph gac = new GreedyAcyclicGraph(parsedGraph);
		gac.run();
		
	
	}
	
	

}
