package de.unileipzig.biovis.test;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import de.unileipzig.biovis.ecm.LayerAssignment;
import de.unileipzig.biovis.ecm.OneLayerCrossMinimization;
import de.unileipzig.biovis.graph.Graph;
import de.unileipzig.biovis.graph.GreedyAcyclicGraph;
import de.unileipzig.biovis.xmlimport.XMLImporter;

public class OLCMTestMain {
	
	private final static List<String> DATA_FILES = Arrays.asList("data/xml/test.graphml","data/xml/Checkstyle-6.5.graphml", "data/xml/JFtp.graphml",
			"data/xml/JUnit-4.12.graphml", "data/xml/Stripes-1.5.7.graphml", "data/xml/test.graphml", "data/xml/testUnconnected.graphml");
	
	public static void main(String[] args) {

		XMLImporter xmlImport = new XMLImporter(new File(DATA_FILES.get(0)));
		Graph parsedGraph = xmlImport.parseToGraph(XMLImporter.EDGE_TYPES.get(2));
		GreedyAcyclicGraph dag = new GreedyAcyclicGraph(parsedGraph);
    	LayerAssignment layerAss = new LayerAssignment(dag);
    	System.out.println("Assigned layers:");
    	for(int i = 0; i < layerAss.getLayers().keySet().size(); i++) {
    		System.out.println(i + ": " + layerAss.getLayers().get(i));
    	}
    	layerAss.setLayers(OneLayerCrossMinimization.barycenterOLCM(layerAss.getLayers()));
    	System.out.println("Assigned layers after OLCM:");
    	for(int i = 0; i < layerAss.getLayers().keySet().size(); i++) {
    		System.out.println(i + ": " + layerAss.getLayers().get(i));
    	}
	}
}
