package de.unileipzig.biovis.test;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import de.unileipzig.biovis.graph.Graph;
import de.unileipzig.biovis.graph.Node;
import de.unileipzig.biovis.xmlimport.XMLImporter;

public class TestMain {

	private final static List<String> DATA_FILES = Arrays.asList("data/xml/Checkstyle-6.5.graphml", "data/xml/JFtp.graphml",
			"data/xml/JUnit-4.12.graphml", "data/xml/Stripes-1.5.7.graphml");
	
	public static void main(String[] args) {

		XMLImporter xmlImport = new XMLImporter(new File(DATA_FILES.get(2)));
		Graph graph = xmlImport.parseToGraph(XMLImporter.EDGE_TYPES.get(0));
		//System.out.println(graph.getEdges());
		System.out.println(graph.getEdges().size());
		List<Node> nodes = graph.getNodes();
		int i=0;
		for (Node node : nodes){
			//if(node.getEdgesIn().isEmpty()){
			if(node.getEdgesOut().isEmpty()){
				System.out.println(node.getName());
				i++;
			}
		}
		System.out.println(i);
		System.out.println(nodes.size());
	}
	
}
