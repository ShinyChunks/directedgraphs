package de.unileipzig.biovis.test;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import de.unileipzig.biovis.ecm.LayerAssignment;
import de.unileipzig.biovis.ecm.MultiLayerCrossMinimization;
import de.unileipzig.biovis.ecm.OneLayerCrossMinimization;
import de.unileipzig.biovis.ecm.VertexPositioning;
import de.unileipzig.biovis.graph.DummyNode;
import de.unileipzig.biovis.graph.Edge;
import de.unileipzig.biovis.graph.Graph;
import de.unileipzig.biovis.graph.GreedyAcyclicGraph;
import de.unileipzig.biovis.graph.Node;
import de.unileipzig.biovis.xmlimport.XMLImporter;


public class SugiyamaMain {
	
	private final static List<String> DATA_FILES = Arrays.asList("data/xml/mintest.graphml","data/xml/test.graphml","data/xml/Checkstyle-6.5.graphml", "data/xml/JFtp.graphml",
			"data/xml/JUnit-4.12.graphml", "data/xml/Stripes-1.5.7.graphml", "data/xml/test.graphml");
	
	public static void main(String[] args) {

		XMLImporter xmlImport = new XMLImporter(new File(DATA_FILES.get(1)));
		Graph parsedGraph = xmlImport.parseToGraph(XMLImporter.EDGE_TYPES.get(2));
		System.out.println("Graph: "+parsedGraph.getEdges());
		//List<Edge> edges = parsedGraph.getEdges();
		//AcyclicGraph dag = new AcyclicGraph(parsedGraph);
		GreedyAcyclicGraph dag = new GreedyAcyclicGraph(parsedGraph);
		
		LayerAssignment layerassignment = new LayerAssignment(dag);
		System.out.println("Dag: "+dag.getEdges()+"\n");
		//System.out.println(dag.getNodes());
		/*
		System.out.println(layerassignment.getLayerIndex());
		System.out.println(layerassignment.getLayerList());
		System.out.println(layerassignment.getNodesWithLayer());
		*/
		Map<Integer,LinkedList<Node>> layers = layerassignment.getLayers();
		/*
		for (int key : layers.keySet()){
			System.out.println(key);
			for (Node node : layers.get(key)){
				System.out.println("   "+node.getName());
				
			}
		}
		
		//layerassignment.setDummyNodes();
		
		System.out.println("with DummyNodes");
		
		for (int key : layers.keySet()){
			System.out.println(key);
			for (Node node : layers.get(key)){
				System.out.println("   "+node.getName());
				
			}
		}
		
		/*
		MultiLayerCrossMinimization mlcm = new MultiLayerCrossMinimization(layerassignment, dag);
		Map<Integer, LinkedList<Node>> mlcmlayers = mlcm.globalKLevelCrossReduction();
		layerassignment.setLayers(mlcmlayers);
		layerassignment.setNodeLayers();
		mlcmlayers = layerassignment.getLayers();
		
		System.out.println("\nMLCM:");
		for (int key : mlcmlayers.keySet()){
			System.out.println(key);
			for (Node node : mlcmlayers.get(key)){
				System.out.println("   "+node.getName());
				
			}
		}
		//*/
		
		//System.out.println(parsedGraph.getEdges());

		
		
		
		
		
		List<Edge> finalEdges  = dag.getOriginalDummyEdges(parsedGraph.getEdges());
		System.out.println("final: "+finalEdges);
		
		
		//System.out.println(dag.getEdges());
		/*
		int[][] ecm = layerassignment.getEdgeCuttingMatrix(4,5);
		for (int k=0; k< 2; k++){
			for (int i=0; i<3;i++){
				System.out.println(k + " "+i);
				System.out.println(ecm[k][i]);
			}
		}
		*/
		//layerassignment.setLayers(OneLayerCrossMinimization.barycenterOLCM(layerassignment.getLayers()));
		VertexPositioning vp = new VertexPositioning(layerassignment);
		vp.setxCoord();
		//vp.setLeftAlignments();
		//vp.setAlignments();
		
		
		
		
	}
}
