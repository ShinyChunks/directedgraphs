package de.unileipzig.biovis;

import java.io.IOException;

import de.unileipzig.biovis.ui.MainPanelController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.Window;

public class MainApp extends Application {

	private Stage primaryStage;
//	private String FileType = "";
	public Window mainWindow;

	private BorderPane rootLayout;

	/**
	 * Start method of the java fx main.
	 */
	@Override
	public void start(Stage primaryStage) {
		this.primaryStage = primaryStage;
		initRootLayout();
		this.mainWindow = primaryStage.getOwner();
	}

	/**
	 * Initializes the root layout of the UI.
	 */
	private void initRootLayout() {
	
		try {
			// Load root layout from fxml file.
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("ui/MainPanel.fxml"));
			rootLayout = (BorderPane) loader.load();

			// Give the controller access to the main app.
			MainPanelController controller = loader.getController();
			controller.setMainApp(this);

			// Show the scene containing the root layout.
			Scene scene = new Scene(rootLayout);
			scene.getStylesheets().add(MainApp.class.getResource("application.css").toString());
			primaryStage.setScene(scene);
//			primaryStage.setResizable(false);
			primaryStage.setTitle("Graph Visualization");
			primaryStage.getIcons().add(new Image(MainApp.class.getResourceAsStream( "icon.png" )));
			// primaryStage.getIcons().add(new
			// Image(getClass().getResourceAsStream("icon.png")));
			primaryStage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * The main method which starts the application.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		launch(args);
	}

	/**
	 * Closes the application window.
	 */
	public void close() {
		primaryStage.close();
	}
	
	public Stage getPrimaryStage() {
		return primaryStage;
	}
}
